
<!DOCTYPE html>
<html lang="sk">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Odpočívadlo Kľušov</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="../assets/img/logo.jpg" rel="icon">
    <link href="../assets/img/logo.jpg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Font Awesome icons -->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.13.0/js/all.js"></script>

    <!-- CSS File -->
    <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>

<header class="fixed-top" id="header">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="drop-down active"><a href="index.php">Domov</a>
                    <ul>
                        <li><a href="index.php#about">O nás</a></li>
                        <li><a href="index.php#services">Ponuka</a></li>
                        <li><a href="index.php#faq">Vaše otázky</a></li>
                        <li><a href="index.php#subs">Odber noviniek</a></li>
                    </ul>
                </li>
                <li class="drop-down"><a href="index.php#portfolio">Galéria</a>
                    <ul>
                        <li><a href="gallery.php">Celá galéria</a></li>
                    </ul>
                </li>
                <li class="drop-down"><a href="index.php#testimonials">Recenzie</a>
                    <ul>
                        <li><a href="../pages/reviews.php">Pridanie recenzie</a></li>
                    </ul>
                </li>
                <li><a href="contact.php">Kontakt</a></li>
                <li><a href="documents.php">Dokumenty</a></li>
                <?php
                if (isset($_SESSION["username"])) {
                    ?>
                    <li><a href='profile.php'><i class='fas fa fa-user-edit'></i>&nbsp;&nbsp;Profil</a></li>
                    <li><a href='../forms/users/logout.php'><i
                                class='fas fa fa-user-times'></i>&nbsp;&nbsp;Odhlásenie</a>
                    </li>

                    <?php
                } else {
                    ?>
                    <li><a href='forms.php'><i class="fas fa-user"></i>&nbsp;&nbsp;Prihlásenie</a></li>
                    <?php
                }
                ?>
            </ul>
        </nav>
    </div>

</header>
