<?php
include '..\forms\db_connection.php';
session_start();
include 'header.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Dokumenty</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Dokumenty</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="faq section-bg" id="faq">
        <div class="container">
            <div class="section-title" data-aos="fade-up">
                <h2>Dôležité Dokumenty</h2>
            </div>
            <div class="faq-list">
                <ul>
                    <li data-aos="fade-up">
                        <i class="bx bx-help-circle icon-help"></i>
                        <a class="collapse" data-toggle="collapse" href="#faq-list-1">Obchodné podmienky
                            <i class="bx bx-chevron-down icon-show"></i>
                            <i class="bx bx-chevron-up icon-close"></i>
                        </a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-1">
                            <p>
                            <p>Tieto Všeobecné obchodné podmienky (ďalej len VOP) upravujú práva a
                                povinnosti spoločnosti Odpočívadlo Kľušov s. r. o., IČO: 52 115 917, so sídlom v
                                Bardejove 085
                                01, Cintorínska 613/2, (ďalej len Odpočívadlo Kľušov s. r. o. alebo predávajúci) a
                                kupujúceho
                                tovaru zo sortimentu spoločnosti Odpočívadlo Kľušov s. r. o. kupujúcemu na základe jeho
                                objednávky.</p>
                            <p>Tieto VOP sú záväzné pre všetkých kupujúcich spoločnosti Odpočivadlo
                                Kľušov s. r. o. , ktorí si u nej objednanú tovar.<br>
                                Tieto VOP vstupujú do platnosti dňom ich zverejnenia na internetových stránkach
                                spoločnosti
                                Odpočívadlo Kľušov s.r.o www.odpocivadloklusov.sk. Do týchto VOP je možné nahliadnuť aj
                                v sídle
                                spoločnosti Odpočivadlo Kľušov s. r. o.<br>
                                Všetky vzťahy medzi spoločnosťou Odpočívadlo Kľušov s. r. o. a kupujúcim, ktoré nie sú
                                týmito
                                VOP upravené, sa riadia príslušnými ustanoveniami Obchodného zákonníka.<br> Ak je
                                kupujúci
                                spotrebiteľom v zmysle ustanovenia § 52 ods. 4 Občianskeho zákonníka, teda ak sa jedná o
                                kupujúceho, ktorý pri uzatváraní a plnení zmluvy nejedná v rámci svojej obchodnej alebo
                                inej
                                podnikateľskej činnosti, riadia sa vzťahy neupravené týmito VOP príslušnými
                                ustanoveniami
                                Občianskeho zákonníka.<br>
                                Údaje prezentovaného tovaru na nákupnom portále (najmä dostupnosť tovaru, cena, rozmery,
                                váha,
                                objem, výška zásoby a pod.) nie sú aktualizované spoločnosťou Odpočívadlo Kľušov s. r.
                                o. online
                                v každom okamihu. Tieto údaje ohľadne tovaru nemusia byť na internetovej stránke
                                spoločnosti
                                vždy správne a úplné v porovnaní so skutočným stavom.<br> Tovar nie je možné vždy vopred
                                ukázať
                                kupujúcemu. Tovar predávaný cez internetovú stránku spoločnosti je vždy limitovaný
                                aktuálnymi
                                skladovými zásobami spoločnosti Odpočívadlo Kľušov s. r. o..<br> Na každú takúto
                                mimoriadnu
                                skutočnosť nemožnosti dodania tovaru bude vždy kupujúci informovaný vhodným spôsobom a v
                                prípade
                                uzatvorenej zmluvy má každá zo strán právo od zmluvy odstúpiť.<br>
                                V prípade ak má spoločnosť Odpočívadlo Kľušov s. r. o. uzatvorenú s kupujúcim samostatnú
                                písomnú
                                zmluvu, majú prednosť zmluvné podmienky zakotvené v tejto samostatnej písomnej zmluve
                                pred
                                VOP</p>
                            <p>Kupujúci je podnikateľský subjekt alebo fyzická osoba – občan (ďalej len
                                kupujúci), ktorý v súlade s týmito VOP doručí akýmkoľvek spôsobom (telefonicky, osobne,
                                poštou,
                                e-mailom, prostredníctvom obchodného zástupcu spoločnosti alebo prostredníctvom
                                internetovej
                                stránky spoločnosti Odpočívadlo Kľušov s. r. o. www.odpocivadloklusov.sk) objednávku
                                tovaru do
                                spoločnosti Odpočívadlo Kľušov s. r. o..<br>
                                Tovarom sa rozumejú produkty spoločnosti Odpočívadlo Kľušov s. r. o., alebo tovary iných
                                výrobcov, ktoré sú uvedené v aktuálnej ponuke spoločnosti, ako aj doplnkové služby
                                poskytované
                                spoločnosťou Odpočívadlo Kľušov s. r. o. (ďalej len tovar).<br>
                                Plnením sa rozumie dodanie tovaru spoločnosťou Odpočívadlo Kľušov s. r. o. v súlade s
                                jej
                                predmetom podnikania (ďalej len plnenie) kupujúcemu.<br>
                                Objednávka je jednostranný právny úkon kupujúceho, vykonaný voči spoločnosti Odpočívadlo
                                Kľušov
                                s. r. o., s cieľom obdržať od spoločnosti plnenie.<br>
                                Mimokatalógový tovar je tovar upravený podľa priania kupujúceho, alebo tovar, ktorý sa
                                nenachádza v katalógoch spoločnosti Odpočívadlo Kľušov s. r. o. a bol zabezpečený pre
                                kupujúceho
                                na základe jeho písomnej, telefonickej alebo e-mailovej požiadavky alebo požiadavky
                                uskutočnenej
                                prostredníctvom obchodného zástupcu spoločnosti.<br>
                                Obchodný zástupca je interný zamestnanec spoločnosti Odpočívadlo Kľušov s. r. o. , ktorý
                                sa
                                stará o zverených kupujúcich, a to najmä osobne prijíma od kupujúceho objednávky,
                                poskytuje
                                kupujúcemu poradenstvo, vypracováva kupujúcemu cenové ponuky, prijíma a spolupodieľa sa
                                na
                                riešení reklamácií od kupujúceho, informuje kupujúceho o novinkách v sortimente
                                spoločnosti a
                                pod.</p>
                            <p>Predávajúci má právo podľa charakteru objednaných produktov vyzvať
                                spotrebiteľa k osobnej prehliadke a osobnému odberu produktu (vzťahuje sa hlavne na
                                finančne
                                náročnejší produkt, alebo náročnejší z hľadiska objemu alebo hmotnosti).<br> V týchto
                                prípadoch
                                bude
                                spotrebiteľ kontaktovaný a bude s ním dohodnutý ďalší postup.<br> Pokiaľ spotrebiteľ už
                                uhradil
                                kúpnu cenu, bude mu táto čiastka prevedená späť, pokiaľ sa nedohodnú inak.<br>
                                Predávajúci si vyhradzuje právo zmeny ceny.<br> Pri zmene ceny predávajúci kontaktuje
                                spotrebiteľa.<br>
                                Spotrebiteľ má právo pri takto upravených cenách objednávku stornovať.<br>
                                Bez ohľadu na ostatné ustanovenia zmluvy, predávajúci nezodpovedá spotrebiteľovi za ušlý
                                zisk,
                                stratu príležitostí alebo žiadne iné nepriame alebo následné straty v dôsledku
                                nedbalosti,
                                porušenia zmluvy alebo vzniknuté iným spôsobom.<br>
                                Tieto všeobecné obchodné podmienky boli formulované a ustanovené v dobrej viere, za
                                účelom
                                splnenia zákonných podmienok a úprav korektných obchodných vzťahov medzi predávajúcim a
                                spotrebiteľom. V prípade, ak sa preukážu kompetentným orgánom Slovenskej republiky
                                niektoré
                                ustanovenia týchto podmienok ako neplatné alebo nevynútiteľné, a to celkom alebo
                                čiastočne,
                                platnosť a vynútiteľnosť ostatných ustanovení a zvyšné časti príslušného ustanovenia tým
                                zostávajú nedotknuté.<br>
                                Práva spotrebiteľa vo vzťahu k predávajúcemu vyplývajúce zo zákona o ochrane
                                spotrebiteľa č.
                                634/1992 Zb. v znení neskorších zmien a predpisov a zákona o ochrane spotrebiteľa pri
                                podomovom
                                predaji a zásielkovom predaji č. 108/2000 Z.z v znení neskorších zmien a predpisov,
                                zostávajú
                                týmito podmienkami nedotknuté.<br>
                                Právne vzťahy a podmienky tu výslovne neupravené ako aj prípadné spory vzniknuté z
                                neplnenia
                                týchto podmienok sa riadia príslušnými ustanoveniami Obchodného alebo Občianskeho
                                zákonníka.<br>
                                Predávajúci a spotrebiteľ sa dohodli, že plne uznávajú komunikáciu na diaľku –
                                telefonickú,(vrátane ručne vypísanej objednávky), elektronickú formu komunikácie, najmä
                                prostredníctvom elektronickej pošty a internetovej siete ako platnú a záväznú pre obe
                                zmluvné
                                strany.<br>
                                Tieto Obchodné podmienky vstupujú do platnosti 10. januára 2019.
                            </p>
                        </div>
                    </li>

                    <li data-aos="fade-up">
                        <i class="bx bx-help-circle icon-help"></i>
                        <a class="collapsed" data-toggle="collapse" href="#faq-list-2">Ochrana súkromia
                            <i class="bx bx-chevron-down icon-show"></i>
                            <i class="bx bx-chevron-up icon-close"></i>
                        </a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-2">
                            <p>
                            <p>Prevádzkovateľom internetovej stránky www.odpocivadloklusov.sk je
                                Odpočívadlo Kľušov s.r.o., Cintorínska 613/2, 085 01 Bardejov, IČO: 52 115 917.</p>
                            <p>Aby sme vám mohli poskytnúť všetky služby, potrebujeme o vás vedieť
                                základné informácie.
                                Niektoré z nich majú povahu osobných údajov v zmysle zákona č. 122/2013 Z.z., o ochrane
                                osobných
                                údajov (ďalej len ZnOOÚ).<br>
                                My sa týmto zákonom pri používaní vašich údajov riadime.<br>
                                Zaväzujeme sa, že budeme s vašimi osobnými údajmi zaobchádzať a nakladať v
                                súlade s platnými právnymi predpismi SR.<br>
                                Vyhlasujeme, že budeme spracúvať osobné údaje v súlade s dobrými mravmi a budeme konať
                                spôsobom,
                                ktorý neodporuje ZnOOÚ ani iným všeobecne záväzným právnym predpisom a ani ich nebudeme
                                obchádzať. <br>Vyhlasujeme, že Váš súhlas si nebudeme vynucovať a ani podmieňovať
                                hrozbou
                                odmietnutia zmluvného vzťahu, služby, tovaru alebo povinnosti nami ustanovenej.<br>
                            </p>
                            <p>Vaše osobné údaje nezverejňujeme, nesprístupňujeme, neposkytujeme žiadnym
                                iným subjektom.</p>
                            <p>V súvislosti so spracúvaním vašich osobných údajov sa predpokladá podľa
                                ust. § 15 ods. 1 písm. e) bod 3 a bod 4 ZnOOÚ, že údaje budú poskytnuté a sprístupnené
                                nasledovným tretím stranám, resp. okruhu príjemcov:
                            <ul>
                                <li>Slovenská pošta, a.s., so sídlom Partizánska cesta 9, 975 99 Banská
                                    Bystrica, IČO: 36631124, zapísaná v Obchodnom registri Okresného súdu Banská
                                    Bystrica,
                                    Oddiel Sa, Vložka č.803/S
                                </li>
                                <li>GLS General Logistics Systems Slovakia s.r.o., Lieskovská cesta 13,
                                    962 21 Lieskovec, IČO: 36624942, zapísaná v Obchodnom registri Okresného súdu Banská
                                    Bystrica, Oddiel Sro, Vložka č.9084/S
                                </li>
                            </ul>
                            <p>Registráciou zároveň potvrdzujete, že sme vás v zmysle platnej
                                legislatívy informovali o tom, že ako predávajúci budeme v procese uzatvárania a plnenia
                                kúpnej
                                zmluvy spracúvať vaše osobné údaje bez vášho súhlasu ako dotknutej osoby, keďže
                                spracúvanie
                                vašich osobných údajov bude vykonávané v predzmluvných vzťahoch a spracúvanie vašich
                                osobných
                                údajov je nevyhnutné na vybavenie objednávky, dodanie tovaru a plnenie kúpnej zmluvy, v
                                ktorej
                                vystupujete ako jedna zo zmluvných strán.<br>
                                Zaškrtnutím príslušného políčka „Súhlasím so spracúvaním mojich osobných údajov“ pri
                                registrácii
                                vyjadrujete svoj dobrovoľný súhlas so spracúvaním a uchovávaním Vašich osobných údajov,
                                ktoré
                                ste uviedli pri Vašej registrácii, ako aj údajov súvisiacich s Vaším používaním Obchodu
                                (ako
                                napríklad údaje o navštívených sekciách Obchodu, údaje o prezeraných a nakúpených
                                produktoch, a
                                pod.), na účely priameho marketingu, zasielania informácií o nových produktoch, zľavách
                                a
                                akciách na ponúkaných tovaroch a iných marketingových aktivitách a zlepšovania ponuky
                                našich
                                služieb.<br>
                                Máte právo na základe písomnej žiadosti namietať voči spracúvaniu vašich osobných
                                údajov, o
                                ktorých predpokladáte, že sú alebo budú použité pre účely priameho marketingu bez Vášho
                                súhlasu
                                a môžete žiadať o ich výmaz.<br> Máte právo namietať aj voči využívaniu a poskytovaniu
                                vašich
                                osobných údajov na účely priameho marketingu (aj v poštovom styku).<br>
                                Svoj súhlas môžete taktiež kedykoľvek odvolať zaslaním písomnej žiadosti na e-mailovú
                                adresu
                                uvedenú na stránke autokomplexbardejov.sk. <br>Bezodkladne po obdržaní odvolania súhlasu
                                so
                                spracúvaním osobných údajov R 66, s.r.o. zabezpečí blokovanie a likvidáciu Vašich
                                osobných
                                údajov, ktoré boli predmetom spracúvania na marketingové účely.<br> V prípade, ak ste sa
                                rozhodli,
                                udeliť nám súhlas na spracúvanie Vašich údajov na marketingové účely, súčasne
                                potvrdzujete, že
                                tento súhlas ste nám udelili dobrovoľne a ste si vedomí/á, že tento súhlas platí až do
                                jeho
                                odvolania, prípadne do skončenia doby spracúvania Vašich osobných údajov.<br>
                                Doba zhromažďovania a spracúvania osobných údajov sa zhoduje s dobou trvania Vašej
                                registrácie
                                na stránkach www.odpocivadloklusov.sk.<br> Na účely fakturácie úhrad, evidencie a
                                vymáhania a
                                postupovania pohľadávok za poskytnutú službu, na účely vybavenia podaní účastníka, na
                                uplatnenie
                                práv alebo na splnenie iných povinností uložených všeobecne záväznými právnymi predpismi
                                je R 66
                                s.r.o. oprávnený viesť evidenciu osobných údajov aj po zániku zmluvy o poskytovaní
                                služby,
                                ktorej ste boli účastníkom.<br>
                                Ako dotknutá osoba môžete u nás písomne uplatniť všetky Vaše práva podľa ustanovení § 28
                                a § 29
                                ZnOOÚ. Ak máte podozrenie, že Vaše osobné údaje sú neoprávnene spracúvané, máte právo
                                podať
                                návrh na začatie konania o ochrane osobných údajov na Úrad na ochranu osobných údajov
                                Slovenskej
                                republiky, so sídlom Hraničná 12, 820 07 Bratislava.</p>
                        </div>
                    </li>

                    <li data-aos="fade-up">
                        <i class="bx bx-help-circle icon-help"></i>
                        <a class="collapse" data-toggle="collapse" href="#faq-list-3">Cookies
                            <i class="bx bx-chevron-down icon-show"></i>
                            <i class="bx bx-chevron-up icon-close"></i>
                        </a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-3">
                            <p>
                            <p>Súbor cookie je malý textový súbor, ktorý webová lokalita ukladá vo vašom počítači, alebo
                                mobilnom zariadení pri jej prehliadaní. Vďaka tomuto súboru si webová lokalita na určitý
                                čas uchováva informácie o vašich krokoch a preferenciách, takže ich pri ďalšej návšteve
                                lokality alebo prehliadaní jej jednotlivých stránok nemusíte opätovne uvádzať.</p>
                            <p>Táto web stránka používa súbory cookies na zapamätanie používateľských nastavení. Rovnako
                                aj pre lepšie prispôsobenie záujmom návštevníkov a pre nevyhnutnú funkčnosť stránky.<br>
                                Používam aj nástroj Google Analytics a Adwords. Tieto služby umožňujú webovú analýzu,
                                ktorej poskytovateľom je spoločnosť Google.<br>
                                Súbor cookie generuje informácie o tom, ako sú webové stránky využívané a aká je ich
                                návštevnosť. Ukladajú sa do priečinka prehliadača a následne sú odoslané a uložené na
                                serveroch Google. Vo všeobecnosti zjednodušujú prehliadanie webu.<br>
                                Spoločnosť Google používa tieto informácie pre analýzu aktivít na webových stránkach a
                                tiež pre tvorbu prehľadov.<br>

                                Pomáhajú skvalitňovať poskytované služby, a to prispôsobením obsahu stránok potrebám a
                                záujmom užívateľov. Ukladaním vašich predvolieb vyhľadávania či registrácie do nových
                                služieb, pomáhajú chrániť údaje a vyberať relevantné reklamy.<br>
                                Spoločnosť Google je oprávnená poskytnúť tieto informácie tretím stranám, ak to vyžaduje
                                zákon, alebo v prípade že takéto tretie strany spracovávajú dáta v mene spoločnosti
                                Google.<br>
                                Na stránke sú aj odkazy na iné stránky ( ako YouTube, Katasterportal, Ministerstvo
                                financií a pod..), ktoré môžu rovnako využívať cookies.<br>
                            <p>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

</main>

<?php
include 'footer.php';
?>

<!-- JS File -->
<script src="../js/main.js"></script>

</body>
</html>