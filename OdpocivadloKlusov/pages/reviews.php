<?php
include '..\forms\db_connection.php';
date_default_timezone_set('Europe/Bratislava');
session_start();
include 'header.php';
?>

<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Recenzie</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Recenzie</li>
                </ol>
            </div>
        </div>
    </section>
    <br>
    <?php
    if (isset($_GET['messageSuccess'])) {
        $message = $_GET['messageSuccess']; ?>
        <p class='alert alert-success text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    if (isset($_GET['messageError'])) {
        $message = $_GET['messageError']; ?>
        <p class='alert alert-danger text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    ?>

    <div class="container">
        <div class="login-form">
            <div class="jumbotron">
                <h3 data-aos='fade-up'>Prehľad recenzií</h3><br>
                <div class="text-center" data-aos='fade-up'>
                    <?php
                    if (isset($_SESSION['username'])) {
                        $username = $_SESSION['username'];
                        $sql = "SELECT * FROM users WHERE username='$username';";
                        $result = mysqli_query($conn, $sql);
                        $rowusers = mysqli_fetch_array($result);
                        if (strpos($rowusers['userType'], "admin") !== false) { ?>
                            <small class="text-muted">
                                Ste prihásený ako ADMIN. Môžete vymazať či upraviť recenziu akéhokoľvek
                                používateľa.
                            </small>
                            <?php
                        }
                    } else { ?>
                        <small class="text-muted">
                            Ste prihásený ako POUŽÍVATEĽ. Môžete vymazať či upraviť len svoje recenzie.
                        </small>
                        <?php
                    }
                    ?>
                </div>
                <hr>
                <div class="testimonials">
                    <br>
                    <?php
                    if (isset($_SESSION["username"])) {
                    $username = $_SESSION['username'];
                    $sql = "SELECT * FROM users WHERE username='$username';";
                    $result = mysqli_query($conn, $sql);
                    $rowusers = mysqli_fetch_array($result);
                    ?>
                    <div class='text-center'>
                        <button type='submit' name='review-add' data-aos='fade-up' class='btn-register'
                                data-toggle='modal' data-target='#modal-add-review'
                        >Pridať
                            recenziu
                        </button>
                    </div>
                    <!-- ADD-->
                    <?php
                    $sql = "SELECT * FROM reviews ORDER BY id DESC;";
                    $result = $conn->query($sql);
                    while ($rowreviews = $result->fetch_assoc()) {
                    echo "<div class='testimonial-item' data-aos='fade-up'>";
                    echo "<h3>" . $rowreviews['uid'] . "</h3>";
                    echo "<h4>Zákazník</h4>";
                    echo "<h4>" . $rowreviews['date'] . "</h4><p>";
                    echo "<i class='bx bxs-quote-alt-left quote-icon-left'></i>";
                    echo nl2br($rowreviews['message']);
                    echo "<i class='bx bxs-quote-alt-right quote-icon-right'></i></p>";

                    if (strcmp($rowusers['id'], $rowreviews['user_id']) == 0 || (strpos($rowusers['userType'], "admin") !== false)) {
                    ?>
                    <div class="edit-form">
                        <!-- EDIT-->
                        <div class='text-center'>
                            <form method="post" action="editreview.php">
                                <?php
                                echo "            <input type='hidden' name='id' value='" . $rowreviews['id'] . "'>";
                                echo "            <input type='hidden' name='uid' value='" . $rowreviews['uid'] . "'>";
                                echo "            <input type='hidden' name='date' value='" . $rowreviews['date'] . "'>";
                                echo "            <input type='hidden' name='message' value='" . $rowreviews['message'] . "'>";
                                echo "            <input type='hidden' name='userid' value='" . $rowreviews['user_id'] . "'>";
                                ?>
                                <button type='submit' name='edit-btn'>Upraviť
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="delete-form">
                        <!--DELETE-->
                        <div class='text-center'>
                            <button name='delete-btn' data-id="<?php echo $rowreviews['id']; ?>"
                                    onclick="confirmDelete(this);">Vymazať
                            </button>
                        </div>
                    </div>
                </div>
                <?php
                }
                }
                ?>
            </div>
            <?php
            } else {
                ?>
                <div class='text-center'>

                    <small class="text-muted" data-aos='fade-up'>
                        Pridať recenziu môže len prihlásený používateľ
                    </small><br><br>
                    <input data-aos='fade-up' onclick="location.href='forms.php'" class='btn-register'
                           type='submit'
                           Value='Prihlásiť sa'>
                </div>
                <?php
                $sql = "SELECT * FROM reviews;";
                $result = $conn->query($sql);
                while ($rowreviews = $result->fetch_assoc()) {
                    echo "<div class='testimonial-item'>";
                    echo "<h3>" . $rowreviews['uid'] . "</h3>";
                    echo "<h4>Zákazník</h4>";
                    echo "<h4>" . $rowreviews['date'] . "</h4><p>";
                    echo "<i class='bx bxs-quote-alt-left quote-icon-left'></i>";
                    echo nl2br($rowreviews['message']);
                    echo "<i class='bx bxs-quote-alt-right quote-icon-right'></i></p>";
                    echo "</div>";
                }
            }
            ?>
        </div>
    </div>
    </div>
</main>

<?php
include 'footer.php';
?>

<div class="modal fade" id="modal-add-review" tabindex="-1" role="dialog" aria-labelledby="modalLabelAdd">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelAdd">Pridanie recenzie</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <?php
                if (isset($_SESSION['username'])) {
                    ?>
                    <form method="post" action="../forms/review.php">
                        <div class="justify-content-center" data-aos="fade-up">
                            <?php
                            if (isset($_SESSION["username"])) {
                                $username = $_SESSION['username'];
                                $sql = "SELECT * FROM users WHERE username='$username';";
                                $result = mysqli_query($conn, $sql);
                                $rowusers = mysqli_fetch_array($result);
                            }
                            ?>
                            <div class="form-group">

                                <?php
                                if (isset($_SESSION["username"])) {
                                    echo "<label>Odosielateľ</label>";
                                    echo " <input type='hidden' name='userid' class='form-control' value='" . $rowusers['id'] . "' >";
                                    echo " <input type='text' name='uid' class='form-control' value='" . $rowusers['first_name'] . " " . $rowusers['last_name'] . "' readonly>";
                                } ?>
                            </div>
                            <div class='form-group'>
                                <?php
                                echo " 
                                <input type='hidden' name='date' value='" . date('Y-m-d H:i:s') . "'>
                                ";
                                ?>
                            </div>
                            <div class='form-group'></div>
                            <label>Text recenzie</label>
                            <textarea class='form-control review-text' name='message' placeholder='Správa' data-emojia
                                      rows='5' minlength='10'
                                      required></textarea>
                        </div>
                        <br>
                        <div class='text-center'>
                            <button type='submit' name='review-submit' data-aos="fade-up" class='btn-register'>
                                Odoslať
                                recenziu
                            </button>
                        </div>
                        <br>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete-review" tabindex="-1" role="dialog" aria-labelledby="modalLabelDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelDelete">Vymazanie recenzie</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Určite chcete vymazať túto recenziu ?</p>
                <form method="POST" action="../forms/review.php" id="form-delete-user">
                    <input type="hidden" name="id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                <button type="submit" form="form-delete-user" name="review-delete" class="btn btn-danger">Vymazať
                </button>
            </div>
        </div>
    </div>
</div>


<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/delete.js"></script>


</body>
</html>
