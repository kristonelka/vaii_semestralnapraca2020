<?php
include '..\forms\db_connection.php';
session_start();
include 'header2.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Profil používateľa</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Profil</li>
                </ol>
            </div>
        </div>
    </section>
    <br>
    <?php
    if (isset($_GET['messageSuccess'])) {
        $message = $_GET['messageSuccess']; ?>
        <p class='alert alert-success text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    if (isset($_GET['messageError'])) {
        $message = $_GET['messageError']; ?>
        echo "<p class='alert alert-danger text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    ?>

    <?php
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM users WHERE username='$username';";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {

        ?>
        <section class="profile" id="about">
            <div class="container">
                <div class="main-body">
                    <div class="jumbotron">
                        <div class="row gutters-sm">
                            <div class="col-md-4 mb-3">
                                <div class="card" data-aos='fade-up'>
                                    <div class="card-body">
                                        <div class="d-flex flex-column align-items-center text-center">
                                            <?php
                                            if (strcmp("Muž", $row['gender']) == 0) {
                                                echo "<img alt='' class='profile-img img-fluid'  src='../assets/profilepictures/male.png'>";
                                            } else {
                                                echo "<img alt='' class='profile-img img-fluid'  src='../assets/profilepictures/female.png'>";
                                            }
                                            ?>
                                            <hr>
                                            <div class="mt-3">
                                                <?php
                                                echo "<h4>" . $row['username'] . "</h4>";
                                                echo "<p class='text-secondary mb-1'>" . $row['userType'] . "</p>";
                                                ?>
                                            </div>
                                            <div class="mt-3">
                                                <div class="text-center">
                                                    <form class='profile-form' method='POST' action='editprofile.php'>
                                                        <button class='btn-register' data-aos='fade-up'>Upraviť</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card mb-3" data-aos='fade-up'>
                                    <div class="card-body">
                                        <h3>Osobné informácie</h3><br><br>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Meno</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'>" . $row['first_name'] . "</div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Priezvisko</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'>" . $row['last_name'] . "</div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'>" . $row['mail'] . "</div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Dátum narodenia</h6>
                                            </div>
                                            <div class='col-sm-9 text-secondary'>
                                                <?php if ($row['dob'] == '0000-00-00') {
                                                    echo "<small>doplňte dátum narodenia</small>";
                                                } else {
                                                    echo $row['dob'];
                                                } ?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Vek</h6>
                                            </div>
                                            <?php
                                            if ($row['dob'] === '0000-00-00') {
                                                echo "<div class='col-sm-9 text-secondary'><small>X rokov</small></div>";
                                            } else {
                                                $birthDate = explode("-", $row['dob']);
                                                //get age from date or birthdate
                                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                                    ? ((date("Y") - $birthDate[0]) - 1)
                                                    : (date("Y") - $birthDate[0]));

                                                echo "<div class='col-sm-9 text-secondary'>" . $age . " rokov</div>";
                                            }

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Telefónne číslo</h6>
                                            </div>
                                            <div class='col-sm-9 text-secondary'>
                                                <?php if ($row['phone'] == NULL) {
                                                    echo "<small>doplňte telefónne číslo</small>";
                                                } else {
                                                    echo $row['phone'];
                                                } ?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Adresa</h6>
                                            </div>
                                            <div class='col-sm-6 text-secondary'>
                                                <small class="mb-0">Ulica:&nbsp;&nbsp;&nbsp;</small>
                                                <?php if ($row['street'] == NULL) {
                                                    echo "<small>doplňte ulicu</small>";
                                                } else {
                                                    echo $row['street'];
                                                } ?>
                                            </div>
                                            <div class='col-sm-3 text-secondary'>
                                                <small class="mb-0">Číslo:&nbsp;&nbsp;&nbsp;</small>
                                                <?php if ($row['number'] == NULL) {
                                                    echo "<small>doplňte číslo</small>";
                                                } else {
                                                    echo $row['number'];
                                                } ?>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">&nbsp;</h6>
                                            </div>
                                            <div class='col-sm-3 text-secondary'>
                                                <small class="mb-0">Mesto:&nbsp;&nbsp;&nbsp;</small>
                                                <?php if ($row['city'] == NULL) {
                                                    echo "<small>doplňte mesto</small>";
                                                } else {
                                                    echo $row['city'];
                                                } ?>
                                            </div>
                                            <div class='col-sm-3 text-secondary'>
                                                <small class="mb-0">PSČ:&nbsp;&nbsp;&nbsp;</small>
                                                <?php if ($row['PSC'] == NULL) {
                                                    echo "<small>doplňte PSČ</small>";
                                                } else {
                                                    echo $row['PSC'];
                                                } ?>
                                            </div>
                                            <div class='col-sm-3 text-secondary'>
                                                <small class="mb-0">Štát:&nbsp;&nbsp;&nbsp;</small>
                                                <?php if ($row['state'] == NULL) {
                                                    echo "<small>doplňte štát</small>";
                                                } else {
                                                    echo $row['state'];
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    ?>
</main>

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Odpočívadlo Kľušov</h3>
                        <p>
                            Cintorínska 613/2<br> 085 01 Bardejov<br>
                            <strong>Telefón:</strong> +421 908 970 890<br>
                            <strong>Email:</strong> odpocivadloklusov@centrum.sk<br>
                        </p>
                        <div class="social-links mt-3">
                            <a class="facebook" href="https://www.facebook.com/odpocivadlo.klusov"><i
                                        class="bx bxl-facebook"></i></a>
                            <a class="instagram" href="https://www.instagram.com/odpocivadlo_klusov/"><i
                                        class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Užitočné linky</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#header">Domov</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#about">O nás</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Ponuka</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php">Obchodné
                                podmienky</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php">Ochrana súkromia</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Naša ponuka</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#portfolio">Ako to u nás vyzerá</a>
                        </li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Čo ponúkame</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Novinky</h4>

                    <div class="text-center">
                        <button data-aos="fade-up" class="btn-register" onclick="location.href='index.php#footer'">
                            Prihláste sa na odber noviniek
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            Copyright &copy; 2020 <strong><span>Odpočívadlo Kľušov</span></strong>. Všetky práva vyhradené
        </div>
        <div class="credits">
            Designed by Kristína Pavličko
        </div>
    </div>
</footer>

<a class="back-to-top" href="#"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../assets/vendor/venobox/venobox.min.js"></script>
<script src="../assets/vendor/aos/aos.js"></script>

<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/delete.js"></script>

</body>
</html>