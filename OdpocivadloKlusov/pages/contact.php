<?php
include '..\forms\db_connection.php';
session_start();
include 'header.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Kontakt</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Kontakt</li>
                </ol>
            </div>
        </div>
    </section>
<br>
    <?php
    if (isset($_GET['messageSuccess'])) {
        $message = $_GET['messageSuccess']; ?>
        <p class='alert alert-success text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    if (isset($_GET['messageError'])) {
        $message = $_GET['messageError']; ?>
        <p class='alert alert-danger text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    ?>

    <section class="contact" id="contact">
        <div class="container">
            <div class="section-title" data-aos="fade-up">
                <h2>Kontakt</h2>
            </div>
            <div class="row no-gutters justify-content-center" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-stretch">
                    <div class="info">
                        <div class="address">
                            <i class="icofont-google-map"></i>
                            <h4>Adresa:</h4>
                            <p>Cintorínska 613/2, 085 01 Bardejov</p>
                        </div>
                        <div class="email mt-4">
                            <i class="icofont-envelope"></i>
                            <h4>Email:</h4>
                            <p>odpocivadloklusov@centrum.sk</p>
                        </div>
                        <div class="phone mt-4">
                            <i class="icofont-phone"></i>
                            <h4>Telefón:</h4>
                            <p>+421 908 970 890</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-stretch">
                    <iframe allowfullscreen
                            src="https://maps.google.com/maps?q=K%C4%BEu%C5%A1ov%2C%20Odpo%C4%8D%C3%ADvadlo%20K%C4%BEu%C5%A1ov&t=&z=13&ie=UTF8&iwloc=&output=embed"
                            style="border:0; width: 100%; height: 270px;"></iframe>
                </div>
            </div>

            <div class="row mt-5 justify-content-center" data-aos="fade-up">
                <div class="col-lg-10">
                    <div class="jumbotron">
                        <form action="../forms/contact.php" method="post" onsubmit="return tryContact()">
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <input type='text' name='username' class='form-control' id="contact-name"
                                           placeholder='Meno a Priezvisko' required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type='email' name='email' class='form-control' id="contact-email"
                                           placeholder='Email' required>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class='form-control' type="text" name="subject" id="contact-subject"
                                       placeholder="Predmet">
                            </div>
                            <div class="form-group">
                                <textarea class='form-control' name='message' placeholder='Správa' id="contact-message"
                                          rows='5'> </textarea>
                            </div>
                            <div class="text-center">
                                <button type='submit' name='contact-submit' class='btn-register'>Odoslať správu</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<?php
include 'footer.php';
?>

<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/validation.js"></script>

</body>
</html>