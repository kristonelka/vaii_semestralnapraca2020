<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Odpočívadlo Kľušov - Profil</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="../assets/img/logo.jpg" rel="icon">
    <link href="../assets/img/logo.jpg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Font Awesome icons -->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.13.0/js/all.js"></script>

    <!-- CSS File -->
    <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>
<header class="fixed-top" id="header">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <?php
                $username = $_SESSION['username'];
                $sql = "SELECT * FROM users WHERE username='$username';";
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {
                    $userType = $row['userType'];
                    if (strpos($userType, "admin") !== false) { ?>
                        <li class="drop-down active"><a href="#">Možnosti </a>
                            <ul>
                                <li><a href="#about">Osobné údaje</a></li>
                                <li><a href="../forms/tableuserscrud/userspage.php">Registrovaní používatelia</a></li>
                                <li><a href="../forms/tablemessagescrud/messages.php">Prijaté správy</a></li>
                                <li><a href="../forms/tablesubscrud/subscriptions.php">Aktívne odbery</a></li>
                            </ul>
                        </li>
                        <?php
                    }
                }
                ?>
                <li><a href="index.php"><i class="fas fa-2x fa-home"></i></a></li>
                <li><a href='../forms/users/logout.php'><i class='fas fa fa-user-times'></i>&nbsp;&nbsp;Odhlásenie</a>
            </ul>
        </nav>
    </div>
</header>