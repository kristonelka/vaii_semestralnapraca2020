<?php
include '..\forms\db_connection.php';
session_start();
include 'header.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Galéria</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Galéria ponuky</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="portfolio section-bg" id="portfolio">
        <div class="container">
            <div class="section-title" data-aos="fade-up">
                <h2>Galéria našej ponuky</h2>
            </div>
            <br><br>
            <div class="row" data-aos="fade-up">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li class="filter-active" data-filter="*">Všetko</li>
                        <li data-filter=".filter-coffee">Káva</li>
                        <li data-filter=".filter-food">Jedlo</li>
                        <li data-filter=".filter-icecream">Zmrzlina</li>
                        <li data-filter=".filter-nealko">Nealko</li>
                        <li data-filter=".filter-alko">Alko</li>
                        <li data-filter=".filter-tabaco">Tabak</li>
                        <li data-filter=".filter-souvenirs">Suveníry</li>
                        <li data-filter=".filter-tipos">Tipos</li>
                        <li data-filter=".filter-car">Auto</li>
                    </ul>
                </div>
            </div>
            <div class="row portfolio-container" data-aos="fade-up">
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(1).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>

                                <a href="gallery-details.php#kava1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(2).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>

                                <a href="gallery-details.php#kava2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(3).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>

                                <a href="gallery-details.php#kava3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(4).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>

                                <a href="gallery-details.php#kava4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(5).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(5).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#kava5" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-coffee">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(6).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/coffee/coffee%20(6).jpg" title="Kava"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#kava6" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(1).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(2).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(3).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(4).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(5).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(5).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo5" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-food">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(6).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/food/food%20(6).jpg" title="Jedlo"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#jedlo6" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-icecream">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/icecream/icecream%20(1).jpg" title="Zmrzlina"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#zmrzlina1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-icecream">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/icecream/icecream%20(2).jpg" title="Zmrzlina"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#zmrzlina2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-icecream">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/icecream/icecream%20(3).jpg" title="Zmrzlina"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#zmrzlina3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-icecream">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/icecream/icecream%20(4).jpg" title="Zmrzlina"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#zmrzlina4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-nealko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/drinks/drinks%20(1).jpg" title="Nealko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#nealko1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-nealko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/drinks/drinks%20(2).jpg" title="Nealko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#nealko2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-nealko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/drinks/drinks%20(3).jpg" title="Nealko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#nealko3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-nealko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/drinks/drinks%20(4).jpg" title="Nealko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#nealko4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(1).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(2).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(3).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(4).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(5).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(5).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko5" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-alko">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(6).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/alco/alco%20(6).jpg" title="Alko"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#alko6" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-tabaco">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tabaco/tabaco%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/tabaco/tabaco%20(2).jpg" title="Tabak"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#tabak1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-tabaco">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tabaco/tabaco%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/tabaco/tabaco%20(3).jpg" title="Tabak"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#tabak2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-souvenirs">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/suvenirs/suvenirs%20(1).jpg" title="Suveniry"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#suvenir1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-souvenirs">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/suvenirs/suvenirs%20(2).jpg" title="Suveniry"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#suvenir2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-souvenirs">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(3).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/suvenirs/suvenirs%20(3).jpg" title="Suveniry"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#suvenir3" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-souvenirs">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(4).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/suvenirs/suvenirs%20(4).jpg" title="Suveniry"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#suvenir4" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-tipos">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tipos/tipos%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/tipos/tipos%20(1).jpg" title="Tipos"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#tipos1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-tipos">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tipos/tipos%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/tipos/tipos%20(2).jpg" title="Tipos"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#tipos2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-car">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/car/car%20(1).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/car/car%20(1).jpg" title="Auto"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#auto1" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 portfolio-item filter-car">
                    <div class="portfolio-wrap">
                        <img alt="" class="img-fluid" src="../assets/img/pics/car/car%20(2).jpg">
                        <div class="portfolio-info">
                            <div class="portfolio-links">
                                <a class="venobox" data-gall="portfolioGallery"
                                   href="../assets/img/pics/car/car%20(2).jpg" title="Auto"><i
                                            class="bx bx-plus"></i></a>
                                <a href="gallery-details.php#auto2" title="Viac detailov"><i
                                            class="bx bx-link"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php
include 'footer.php';
?>

<!-- JS File -->
<script src="../js/main.js"></script>

</body>
</html>