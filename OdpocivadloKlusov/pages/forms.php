<?php
include '..\forms\db_connection.php';
session_start();
?>
<!DOCTYPE html>
<html lang="sk">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Odpočívadlo Kľušov - Prihlásenie</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="../assets/img/logo.jpg" rel="icon">
    <link href="../assets/img/logo.jpg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Font Awesome icons -->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.13.0/js/all.js"></script>

    <!-- CSS File -->
    <link href="../assets/css/style.css" rel="stylesheet">
    <style>
        #register-box, #forgot-box {
            display: none;
        }
    </style>
</head>
<body>

<header class="fixed-top" id="header">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="active"><a href='index.php'><i class="fas fa-2x fa-home"></i></a></li>
            </ul>
        </nav>
    </div>
</header>

<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Prihlásenie / Registrácia</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Prihlásenie-Registrácia</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="contact" id="contact">
        <div class="container" data-aos="fade-up">
            <div class="container nt-4">

                <!-- Login Form -->
                <div class="row ">
                    <div class="col-lg-6 offset-lg-3 rounded" id="login-box" style="background-color:#ECECEC;">
                        <div class="section-title">
                            <h2 style="padding-top: 20px;">Prihlásenie</h2>
                        </div>
                        <?php
                        if (isset($_GET['messageSuccess'])) {
                            $message = $_GET['messageSuccess']; ?>
                            <p class='alert alert-success text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
                            <?php
                        }
                        if (isset($_GET['messageError'])) {
                            $message = $_GET['messageError']; ?>
                            <p class='alert alert-danger text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
                            <?php
                        }
                        ?>
                        <form action="../forms/users/signin.php" method="post" id="login-frm"
                              onsubmit="return tryLogin()">
                            <div class="form-group">
                                <label>Používateľské meno</label>
                                <input type="text" name="username" placeholder="Používateľské meno" id="login-username"
                                       class="form-control"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Heslo</label>
                                <div class="input-group">
                                    <input id="viewPwdLogin" type="password" name="pass" placeholder="Heslo"
                                           class="form-control" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-eye" id="eye"
                                                                         onclick="changePwdView()"></i></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="text-center">
                                <div class="form-group">
                                    <input type="submit" value="Prihlásiť" name="login" id="login" class="btn-register">
                                </div>
                                <br>
                                <div class="form-group">
                                    <p class="text-center">Nový používateľ ? <a href="#" id="register-btn">Zaregistrujte
                                            sa.</a></p>
                                </div>
                                <p class="text-center"><a href="#" id="forgot-btn">Zabudnuté heslo ?</a></p>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Registration Form -->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 rounded" id="register-box" style="background-color: #ECECEC;">
                        <div class="section-title">
                            <h2 style="padding-top: 20px;">Registrácia</h2>
                        </div>
                        <form action="../forms/users/signup.php" method="post" id="register-frm"
                              onsubmit="return tryRegister()">
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <label>Meno</label>
                                    <input type="text" name="fname" id="register-name" placeholder="Meno"
                                           class="form-control" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Priezvisko</label>
                                    <input type="text" name="lname" placeholder="Priezvisko" id="register-lastname"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Používateľské meno</label>
                                <input type="text" name="username" placeholder="Používateľské meno"
                                       id="register-username" class="form-control"
                                       required>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <label>Email</label>
                                    <input type="email" name="mail" placeholder="Email" id="register-email"
                                           class="form-control" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Dátum narodenia</label>
                                    <input type="date" name="dob" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <label>Heslo</label>
                                    <div class="input-group">
                                        <input id="viewPwdLogin2" type="password" name="pass" placeholder="Heslo"
                                               class="form-control eye" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text"><i class="fas fa-eye" id="eye2"
                                                                             onclick="changePwdView2()"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Potvrdenie hesla</label>
                                    <div class="input-group">
                                        <input id="viewPwdLogin3" type="password" name="cpass" placeholder="Heslo znova"
                                               class="form-control eye" required>
                                        <div class="input-group-append">
                                            <div class="input-group-text"><i class="fas fa-eye" id="eye3"
                                                                             onclick="changePwdView3()"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="col-md-6">
                                    <label>Vyberte pohlavie</label>&nbsp;&nbsp;
                                </div>
                                <div class="col-md-6 ">
                                    <input type="radio" id="optionFemale" name="gender" value="Žena" checked/>
                                    Žena &nbsp;&nbsp;
                                    <input type="radio" id="optionMale" name="gender" value="Muž"/>
                                    Muž
                                </div>
                            </div>
                            <br>
                            <div class="text-center">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="check" class="custom-control-input"
                                               id="customCheck2">
                                        <label for="customCheck2" class="custom-control-label">Súhlasím s<a href="#">
                                                podmienkami.</a></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Registrovať" name="register" id="register"
                                           class="btn-register">
                                </div>
                                <br>

                                <div class="form-group">
                                    <p class="text-center">Už máte účet ? <a href="#" id="login-btn">Prihláste
                                            sa.</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Forgot password -->
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 rounded" id="forgot-box" style="background-color: #ECECEC;">
                        <div class="section-title">
                            <h2 style="padding-top: 20px;">Zabudnuté heslo</h2>
                        </div>
                        <form action="../forms/users/forgotpass.php" method="post" id="forgot-frm"
                              onsubmit="return tryChangePass()">
                            <div class="form-group text-center">
                                <small class="text-muted">
                                    Na zmenu hesla je potrebné zadať Váš e-mail a potvrdiť nové heslo.
                                </small>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="femail" class="form-control" id="forgot-email"
                                       placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>Nové heslo</label>
                                <div class="input-group">
                                    <input id="viewPwdLogin4" type="password" name="newpass" placeholder="Nové heslo"
                                           class="form-control" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-eye" id="eye4"
                                                                         onclick="changePwdView4()"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Potvrdenie nového hesla</label>
                                <div class="input-group">
                                    <input id="viewPwdLogin5" type="password" name="newcpass"
                                           placeholder="Potvrdenie nového hesla"
                                           class="form-control" required>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-eye" id="eye5"
                                                                         onclick="changePwdView5()"></i></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group text-center">
                                <input type="submit" name="forgot" id="forgot" value="Zmeniť heslo"
                                       class="btn-register">
                            </div>
                            <div class="form-group text-center">
                                <a href="#" id="back-btn">Späť</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>


<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Odpočívadlo Kľušov</h3>
                        <p>
                            Cintorínska 613/2<br> 085 01 Bardejov<br>
                            <strong>Telefón:</strong> +421 908 970 890<br>
                            <strong>Email:</strong> odpocivadloklusov@centrum.sk<br>
                        </p>
                        <div class="social-links mt-3">
                            <a class="facebook" href="https://www.facebook.com/odpocivadlo.klusov"><i
                                        class="bx bxl-facebook"></i></a>
                            <a class="instagram" href="https://www.instagram.com/odpocivadlo_klusov/"><i
                                        class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Užitočné linky</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#header">Domov</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#about">O nás</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Ponuka</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php">Obchodné
                                podmienky</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php">Ochrana súkromia</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Naša ponuka</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#portfolio">Ako to u nás vyzerá</a>
                        </li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Čo ponúkame</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Novinky</h4>

                    <div class="text-center">
                        <button data-aos="fade-up" class="btn-register" onclick="location.href='index.php#footer'">
                            Prihláste sa na odber noviniek
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            Copyright &copy; 2020 <strong><span>Odpočívadlo Kľušov</span></strong>. Všetky práva vyhradené
        </div>
        <div class="credits">
            Designed by Kristína Pavličko
        </div>
    </div>
</footer>

<a class="back-to-top" href="#"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../assets/vendor/venobox/venobox.min.js"></script>
<script src="../assets/vendor/aos/aos.js"></script>

<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/switch.js"></script>
<script src="../js/eye.js"></script>
<script src="../js/validation.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>
</html>