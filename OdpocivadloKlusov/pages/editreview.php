<?php
include '..\forms\db_connection.php';
session_start();
date_default_timezone_set('Europe/Bratislava');
include 'header2.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Recenzie</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li>Recenzie</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="reviews">
        <div class="container">
            <div class="login-form">
                <div class="jumbotron">
                    <h3>Editácia recenzie</h3><br>
                    <div class="justify-content-center" data-aos="fade-up">
                        <form action="../forms/review.php" method="post">
                            <?php
                            $id = $_POST['id'];
                            $uid = $_POST['uid'];
                            $date = $_POST['date'];
                            $message = $_POST['message'];
                            $userid = $_POST['userid'];
                            echo "<div class='form-group'>";
                            echo "<input type='hidden' name='id' value='" . $id . "'>";
                            echo "<input type='hidden' name='uid' value='" . $uid . "'>";
                            echo "<input type='hidden' name='date' value='" . $date . "'>";
                            echo "<textarea class='form-control review-text' name='message' rows='5' required>" . $message . "</textarea></div>";
                            echo "<input type='hidden' name='userid' value='" . $userid . "'>";
                            ?>
                            <div class='text-center'>
                                <button type='submit' name='review-edit' class='btn-register'>Uložiť úpravy</button>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<?php
include 'footer.php';
?>
<!-- JS File -->
<script src="../js/main.js"></script>

</body>
</html>