<?php
include '..\forms\db_connection.php';
session_start();
include 'header2.php';
?>


<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Profil používateľa</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li><a href="forms.php">Prihlásenie</a></li>
                    <li><a href="profile.php">Profil</a></li>
                    <li>Profil - editácia</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="profile">
        <div class="container">
            <?php
            $username = $_SESSION['username'];
            $sql = "SELECT * FROM users WHERE username='$username';";
            $result = mysqli_query($conn, $sql);
            $row = mysqli_fetch_array($result);

            ?>

            <div class="main-body">
                <div class="jumbotron">
                    <form action="../forms/profile/editprofile.php" method="post">
                        <div class="row gutters-sm">
                            <div class="col-md-4 mb-3">
                                <div class="card" data-aos='fade-up'>
                                    <div class="card-body">
                                        <div class="d-flex flex-column align-items-center text-center">
                                            <?php
                                            if (strcmp("Muž", $row['gender']) == 0) {
                                                echo "<img alt='' class='profile-im img-fluid' src='../assets/profilepictures/male.png'>";
                                            } else {
                                                echo "<img alt='' class='profile-img img-fluid' src='../assets/profilepictures/female.png'>";
                                            }
                                            ?>
                                            <hr>
                                            <div class='mt-3'>
                                                <?php
                                                echo "<h4>" . $row['username'] . "</h4>";
                                                echo "<p class='text-secondary mb-1'>" . $row['userType'] . "</p>";
                                                ?>
                                            </div>
                                            <div class="mt-3">
                                                <div class="text-center">
                                                    <button type='submit' name='profile-edit' class='btn-register'
                                                            data-aos='fade-up'>
                                                        Uložiť úpravy
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="card mb-3" data-aos='fade-up'>
                                    <div class="card-body">
                                        <h3>Editácia osobných informácií</h3><br><br>
                                        <div class="row">
                                            <?php
                                            echo "<input type='hidden' name='id' value='" . $row['id'] . "'>";
                                            ?>
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Meno</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'><input type='text' name='first_name' placeholder='Meno' class='form-control' value='" . $row['first_name'] . "' required></div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Priezvisko</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'><input type='text' name='last_name' placeholder='Priezvisko' class='form-control' value='" . $row['last_name'] . "' required></div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'><input type='email' name='mail' placeholder='Email' class='form-control' value='" . $row['mail'] . "' required></div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Dátum narodenia</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'><input type='date' name='dob' placeholder='Dátum narodenia' class='form-control' value='" . $row['dob'] . "'></div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Vek</h6>
                                            </div>
                                            <?php
                                            if ($row['dob'] === '0000-00-00') {
                                                echo "<div class='col-sm-9 text-secondary'><small>X rokov</small></div>";
                                            } else {
                                                $birthDate = explode("-", $row['dob']);
                                                //get age from date or birthdate
                                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                                    ? ((date("Y") - $birthDate[0]) - 1)
                                                    : (date("Y") - $birthDate[0]));

                                                echo "<div class='col-sm-9 text-secondary'>" . $age . " rokov</div>";
                                            }

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Telefónne číslo</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-9 text-secondary'><input type='text' name='phone' placeholder='Telefónne číslo' class='form-control' value='" . $row['phone'] . "'></div>";

                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Adresa</h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-6 text-secondary'><small class='mb-0'>Ulica:&nbsp;&nbsp;&nbsp;</small><input type='text' name='street' placeholder='Ulica' class='form-control' value='" . $row['street'] . "'></div>";
                                            echo "<div class='col-sm-3 text-secondary'><small class='mb-0'>Číslo:&nbsp;&nbsp;&nbsp;</small><input type='text' name='number' placeholder='Číslo' class='form-control' value='" . $row['number'] . "'></div>";
                                            ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0"></h6>
                                            </div>
                                            <?php
                                            echo "<div class='col-sm-3 text-secondary'><small class='mb-0'>Mesto:&nbsp;&nbsp;&nbsp;</small><input type='text' name='city' placeholder='Mesto' class='form-control' value='" . $row['city'] . "'></div>";
                                            echo "<div class='col-sm-3 text-secondary'><small class='mb-0'>PSČ:&nbsp;&nbsp;&nbsp;</small><input type='text' name='PSC' placeholder='PSČ' class='form-control' value='" . $row['PSC'] . "'></div>";
                                            echo "<div class='col-sm-3 text-secondary'><small class='mb-0'>Štát:&nbsp;&nbsp;&nbsp;</small><input type='text' name='state' placeholder='Štát' class='form-control' value='" . $row['state'] . "'></div>";
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div>
                    <?php
                    if (strcmp($username, $_SESSION['username']) == 0) {
                        echo "<input type='hidden' name='id' value='" . $row['id'] . "'>";
                        ?>
                        <div class='text-center'>
                            <button class='btn-danger' name='delete-btn-profile' data-aos='fade-up'
                                    style="padding: 15px; width: 500px;"
                                    data-id="<?php echo $row['id']; ?>" onclick="confirmDelete2(this);">
                                Vymazať účet
                            </button>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</main>

<?php
include 'footer.php';
?>

<div class="modal fade" id="modal-delete-profile" tabindex="-1" role="dialog" aria-labelledby="modalLabelDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelDelete">Vymazanie účtu </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Určite chcete vymazať svoj účet ?</p>
                <form method="POST" action="../forms/profile.php" id="form-delete-profile">
                    <input type="hidden" name="id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                <button type="submit" form="form-delete-profile" name="profile-delete" class="btn btn-danger">Vymazať
                </button>
            </div>
        </div>
    </div>
</div>

<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/delete2.js"></script>

</body>
</html>