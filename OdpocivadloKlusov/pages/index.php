<?php
include '..\forms\db_connection.php';
session_start();
include 'header.php';
?>


<section class="d-flex flex-column justify-content-center align-items-center" id="hero">
    <div class="container text-center text-md-left" data-aos="fade-up">
        <h1>Odpočívadlo Kľušov</h1>
        <?php
        if (isset($_SESSION["username"])) {
            echo "<h2>Používateľ &nbsp;<i class='fas fa-user-check'></i> " . $_SESSION["username"] . "&nbsp;, vitajte ...</h2>";
        } else {
            echo '<h2>Vitajte na stránke našej novotvorenej prevádzky ...</h2>';
        }
        ?>
        <a class="btn-get-started scrollto" href="#about">Chcem vedieť viac</a>
    </div>
</section>

<main id="main">
    <section class="about" id="about">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                    <img alt="" class="img-fluid" src="../assets/img/logo2.jpg">
                </div>
                <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                    <h3 data-aos="fade-up">Máme všetko čo potrebujete !</h3>
                    <p data-aos="fade-up">
                        Odpočívadlo Kľušov je novootvorená prevádzka otvorená v máji 2019.
                    </p>
                    <p data-aos="fade-up">
                        Prevádzka zákazníkom ponúka služby našej kaviarne, bagety, paninni, tabakové výrobky, alkohol,
                        zmrzlinu či iné občerstvenie.
                    </p>
                    <p data-aos="fade-up">
                        Pre večne uponáhľaných zákazníkov máme k dispozicií aj kávovar na kávu "so sebou".
                    </p>
                    <p data-aos="fade-up">
                        Ponúkame aj služby stávkovej kancelárie Tipos či predaj IQOS elektronických cigariet.
                        K dispozicií je aj vonkajšia terasa a prestranné parkovisko.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="steps section-bg" id="services">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in">
                    <span><i class="fas fa-4x fa-coffee"></i></span>
                    <h4>Káva</h4>
                    <p>Ponúkame Vám tú najlepšiu kávu značky Montecelio</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="100">
                    <span><i class="fas fa-4x fa-hamburger"></i></span>
                    <h4>Občerstvenie</h4>
                    <p>Vždy čerstvé bagety, paninny či hotdogy.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="200">
                    <span><i class="fas fa-4x fa-glass-whiskey"></i></span>
                    <h4>Alko/Nealko</h4>
                    <p>Široká ponuka alko aj nealko nápojov.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="300">
                    <span><i class="fas fa-4x fa-smoking"></i></span>
                    <h4>Tabakové výrobky</h4>
                    <p>Pre milovníkov tabaku máme ponuku tradičných cigariet, tabaku ale aj IQOS
                        produktov.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="400">
                    <span><i class="fas fa-4x fa-wallet"></i></span>
                    <h4>Tipos</h4>
                    <p>Skúsiť šťastie môžete aj v Tipos-e.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="500">
                    <span><i class="fas fa-4x fa-ice-cream"></i></span>
                    <h4>Zmrzlina</h4>
                    <p>Tá pravá balkánska zmrzlina.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="500">
                    <span><i class="fas fa-4x fa-car"></i></span>
                    <h4>Auto</h4>
                    <p>Ponuka základných kvapalín pre vaše vozidlo.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="500">
                    <span><i class="fas fa-4x fa-gifts"></i></span>
                    <h4>Suveníry</h4>
                    <p>Mnoho tématických hrnčekov, šiltoviek či iných predmetov.</p>
                </div>
                <div class="col-lg-4 col-md-6 content-item" data-aos="fade-in" data-aos-delay="500">
                    <span><i class="fas fa-4x fa-tree"></i></span>
                    <h4>Vonkajšie posedenie</h4>
                    <p>Vonkajšie posedenie v tichej prírode s možnosťou bezplatného parovania.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="portfolio" class="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-5 mb-lg-0" data-aos="fade-right">
                    <ul class="nav nav-tabs flex-column">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                <h4>Káva</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">
                                <h4>Jedlo</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-3">
                                <h4>Zmrzlina</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-4">
                                <h4>Alkoholické nápoje</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-5">
                                <h4>Nealkoholické nápoje</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-6">
                                <h4>Tabakové výrobky</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-7">
                                <h4>Suveníry</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-8">
                                <h4>Tipos</h4>
                            </a>
                        </li>
                        <li class="nav-item mt-2">
                            <a class="nav-link" data-toggle="tab" href="#tab-9">
                                <h4>Auto</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 ml-auto" data-aos="fade-left">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="tab-1">
                            <figure>
                                <img src="../assets/img/pics/coffee/coffee%20(5).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-2">
                            <figure>
                                <img src="../assets/img/pics/food/food%20(5).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-3">
                            <figure>
                                <img src="../assets/img/pics/icecream/icecream%20(4).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-4">
                            <figure>
                                <img src="../assets/img/pics/alco/alco%20(4).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-5">
                            <figure>
                                <img src="../assets/img/pics/drinks/drinks%20(2).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-6">
                            <figure>
                                <img src="../assets/img/pics/tabaco/tabaco%20(3).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-7">
                            <figure>
                                <img src="../assets/img/pics/suvenirs/suvenirs%20(3).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-8">
                            <figure>
                                <img src="../assets/img/pics/tipos/tipos%20(1).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <div class="tab-pane" id="tab-9">
                            <figure>
                                <img src="../assets/img/pics/car/car%20(2).jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button data-aos="fade-up" class="btn-register" onclick="location.href='gallery.php'">Celá
                    galéria
                </button>
            </div>
        </div>
    </section>

    <section class="testimonials" id="testimonials">
        <div class="container">
            <div class="section-title" data-aos="fade-up">
                <h2>Recenzie</h2>
                <p>Ohodnotiť Vašu dobrú či horšiu skúsenosť môžete aj prostrednístvom recenzií.</p>
                <p>Za každú pochvalu či postreh na zlepšienie Vám ďakujeme.</p>
            </div>
            <div class="owl-carousel testimonials-carousel" data-aos="fade-up">
                <?php
                $sql = "SELECT * FROM reviews;";
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {
                    echo "<div class='testimonial-item'>";
                    echo "<h4>" . $row['date'] . "</h4><p>";
                    echo "<i class='bx bxs-quote-alt-left quote-icon-left'></i>";
                    echo nl2br($row['message']);
                    echo "<i class='bx bxs-quote-alt-right quote-icon-right'></i></p>";
                    echo "<h3>" . $row['uid'] . "</h3>";
                    echo "<h4>Zákazník</h4>";
                    echo "</div>";
                }
                ?>
            </div>
        </div>
        <br>
        <div class="text-center">
            <form action='reviews.php'><input data-aos='fade-up' class='btn-register' type='submit'
                                              value='Pridať recenziu'></form>
        </div>
    </section>

    <section class="faq section-bg" id="faq">
        <div class="container">
            <div class="section-title" data-aos="fade-up">
                <h2>Najčastejšie otázky a odpovede</h2>
                <p>V prípade akýchkoľvek otázok, na ktoré ste odpoveď nenašli nás neváhajte <a href="contact.php">kontaktovať.</a>
                </p>
            </div>
            <div class="faq-list">
                <ul>
                    <li data-aos="fade-up">
                        <i class="bx bx-help-circle icon-help"></i> <a class="collapse" data-toggle="collapse"
                                                                       href="#faq-list-1">Kde sa Vaša prevádzka nachádza
                            ?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div class="collapse show" data-parent=".faq-list" id="faq-list-1">
                            <p>
                                Prevádzka sa nachádza v okrese Bardejov za obcou Kľúšov (smerom na Prešov na pravej
                                strane).
                            </p>
                        </div>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="100">
                        <i class="bx bx-help-circle icon-help"></i> <a class="collapsed" data-toggle="collapse"
                                                                       href="#faq-list-2">Robíte donášku ?<i
                                    class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-2">
                            <p>
                                Žial, nie sme reštaurácia či iná prevádzka špecializujúca sa na gastro, malé
                                občerstvenie vo forme bagiet či hot dogov nerozvážame.
                            </p>
                        </div>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="200">
                        <i class="bx bx-help-circle icon-help"></i> <a class="collapsed" data-toggle="collapse"
                                                                       href="#faq-list-3">Kde môžem nájsť menu ?<i
                                    class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-3">
                            <p>
                                Menu obsahuje klasické občerstvenie ako na väčšine benzínových púmp.
                            </p>
                        </div>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="300">
                        <i class="bx bx-help-circle icon-help"></i> <a class="collapsed" data-toggle="collapse"
                                                                       href="#faq-list-4">Je parkovisko platené ?<i
                                    class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-4">
                            <p>
                                Parkovisko je zadarmo pre všetkých návštevníkov.
                            </p>
                        </div>
                    </li>
                    <li data-aos="fade-up" data-aos-delay="400">
                        <i class="bx bx-help-circle icon-help"></i> <a class="collapsed" data-toggle="collapse"
                                                                       href="#faq-list-5">Sú toalety platené, ak nie som
                            zákazník ?<i class="bx bx-chevron-down icon-show"></i><i
                                    class="bx bx-chevron-up icon-close"></i></a>
                        <div class="collapse" data-parent=".faq-list" id="faq-list-5">
                            <p>
                                Toalety sú zadarmo aj v prípade, že nie ste zákazník.
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>

</main>

<div class="cookie-container">
    <p>Používame cookies aby sme sa čo najlepšie prispôsobili Vašim potrebám. Ak budete pokračovať v používaní tejto
        stránky zároveň tým súhlasíte s používaním cookies.</p>
    <p>Pre viac informácií, prečítajte si tento dokument->
        <a href="documents.php">Pravidlá používania cookies</a>
    </p>
    <button class="cookie-button">Súhlasím</button>
</div>

<?php
include 'footer.php'
?>
<!-- JS File -->
<script src="../js/main.js"></script>
<script src="../js/eye.js"></script>
<script src="../js/cookies.js"></script>
<script src="../js/validation.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</body>
</html>