<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Odpočívadlo Kľušov</h3>
                        <p>
                            Cintorínska 613/2<br> 085 01 Bardejov<br>
                            <strong>Telefón:</strong> +421 908 970 890<br>
                            <strong>Email:</strong> odpocivadloklusov@centrum.sk<br>
                        </p>
                        <div class="social-links mt-3">
                            <a class="facebook" href="https://www.facebook.com/odpocivadlo.klusov"><i
                                    class="bx bxl-facebook"></i></a>
                            <a class="instagram" href="https://www.instagram.com/odpocivadlo_klusov/"><i
                                    class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Užitočné linky</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#header">Domov</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#about">O nás</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Ponuka</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php#documents.php">Obchodné
                                podmienky</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="documents.php#documents.php">Ochrana súkromia</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Naša ponuka</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#portfolio">Ako to u nás vyzerá</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="index.php#services">Čo ponúkame</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter" id="subs">
                    <h4>Novinky</h4>
                    <p>Prihláste sa na odber noviniek</p>
                    <form action="../forms/subscribe.php" method="post" onsubmit="return trySub()">
                        <input name='email' type='email' id="sub-email" placeholder='Email'>
                        <input type="submit" value="Odoberať" name="submit" class="btn-register">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            Copyright &copy; 2020 <strong><span>Odpočívadlo Kľušov</span></strong>. Všetky práva vyhradené
        </div>
        <div class="credits">
            Designed by Kristína Pavličko
        </div>
    </div>
</footer>

<a class="back-to-top" href="#"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="../assets/vendor/jquery/jquery.min.js"></script>
<script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../assets/vendor/venobox/venobox.min.js"></script>
<script src="../assets/vendor/aos/aos.js"></script>

