<?php
include '..\forms\db_connection.php';
session_start();
include 'header2.php';
?>

<body>

<header class="fixed-top" id="header">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li><a href="gallery.php">Galéria</a></li>
                <li><a href="index.php"><i class="fas fa-2x fa-home"></i></a></li>
            </ul>
        </nav>
    </div>
</header>

<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Detail produktu</h2>
                <ol>
                    <li><a href="index.php">Domov</a></li>
                    <li><a href="gallery.php">Galéria</a></li>
                    <li>Detail</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="portfolio-details">
        <div class="container">
            <section id="kava1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Espresso Macchiatto</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Káva, teplé nápoje</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výrobok zrnkovej kávy značky Montecelio. </p>
                            <p>V ponuke aj:</p>
                            <ul>
                                <li>Espresso Classic</li>
                                <li>Espresso Macchiato</li>
                                <li>Espresso Ristretto</li>
                                <li>Espresso Lungo</li>
                                <li>Espresso Doppio</li>
                                <li>Espresso Americano</li>
                                <li>Latte Macchiato</li>
                                <li>Cappuccino</li>
                                <li>Írska káva</li>
                                <li>Espressso Tonic</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="kava2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Espresso Tonic</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Káva, teplé nápoje</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výrobok zrnkovej kávy značky Montecelio. </p>
                            <p>V ponuke aj:</p>
                            <ul>
                                <li>Espresso Classic</li>
                                <li>Espresso Macchiato</li>
                                <li>Espresso Ristretto</li>
                                <li>Espresso Lungo</li>
                                <li>Espresso Doppio</li>
                                <li>Espresso Americano</li>
                                <li>Latte Macchiato</li>
                                <li>Cappuccino</li>
                                <li>Írska káva</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="kava3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Espresso Lungo</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Káva, teplé nápoje</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výrobok zrnkovej kávy značky Montecelio. </p>
                            <p>V ponuke aj:</p>
                            <ul>
                                <li>Espresso Classic</li>
                                <li>Espresso Macchiato</li>
                                <li>Espresso Ristretto</li>
                                <li>Espresso Lungo</li>
                                <li>Espresso Doppio</li>
                                <li>Espresso Americano</li>
                                <li>Latte Macchiato</li>
                                <li>Cappuccino</li>
                                <li>Írska káva</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="kava4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Cappuccino</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Káva, teplé nápoje</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výrobok zrnkovej kávy značky Montecelio. </p>
                            <p>V ponuke aj:</p>
                            <ul>
                                <li>Espresso Classic</li>
                                <li>Espresso Macchiato</li>
                                <li>Espresso Ristretto</li>
                                <li>Espresso Lungo</li>
                                <li>Espresso Doppio</li>
                                <li>Espresso Americano</li>
                                <li>Latte Macchiato</li>
                                <li>Cappuccino</li>
                                <li>Írska káva</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(4).jpg">
                    </div>
                </div>
            </section>
            <section id="kava5">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(5).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Latte Macchiato</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Káva, teplé nápoje</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výrobok zrnkovej kávy značky Montecelio. </p>
                            <p>V ponuke aj:</p>
                            <ul>
                                <li>Espresso Classic</li>
                                <li>Espresso Macchiato</li>
                                <li>Espresso Ristretto</li>
                                <li>Espresso Lungo</li>
                                <li>Espresso Doppio</li>
                                <li>Espresso Americano</li>
                                <li>Latte Macchiato</li>
                                <li>Cappuccino</li>
                                <li>Írska káva</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section id="kava6">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Kávovar</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Príslušenstvo, vybavenie prevádzky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Profesionálny pákový kávovar na prípravu čerstvo mletej kávy. </p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/coffee/coffee%20(6).jpg">
                    </div>
                </div>
            </section>

            <section id="jedlo1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Cheesecake</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zákusky, cheesecake</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výborné domáce cheesecake od -> fb: RenčinéDobroty</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="jedlo2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Cukrovinky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Cukrovinky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Rôzne cukrovinky.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="jedlo3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">OSHEE tyčinky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>OSHEE, poteínové tyčinky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Proteínové tyčinky značky OSHEE.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="jedlo4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">OSHEE tyčinky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>OSHEE, poteínové tyčinky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Proteínové tyčinky značky OSHEE.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(4).jpg">
                    </div>
                </div>
            </section>
            <section id="jedlo5">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(5).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Toasty</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Toasty, Pierre Baquette</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Chutné toasty značky Pierre Baquette s možnosťou zapečenia.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="jedlo6">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Bagety</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Bagety, Pierre Baguette</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Každý deň čerstvé bagety značky Pierre Baguette</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/food/food%20(6).jpg">
                    </div>
                </div>
            </section>

            <section id="zmrzlina1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Zmrzlina</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zmrzlina</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Originálna balkánska zmrzlina.</p>
                        </div>
                    </div>

                </div>
            </section>
            <section id="zmrzlina2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Zmrzlina</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zmrzlina</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Originálna balkánska zmrzlina.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="zmrzlina3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Zmrzlina</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zmrzlina</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Originálna balkánska zmrzlina.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="zmrzlina4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Zmrzlina</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zmrzlina</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Originálna balkánska zmrzlina.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/icecream/icecream%20(4).jpg">
                    </div>
                </div>
            </section>
            <section id="nealko1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">OSHEE</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Nealkoholické nápoje, vitamíny, OSHEE</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Široký výber vitamínových nápojov značky OSHEE (plechovkové aj plastové balenie).</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="nealko2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Aquarius</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Nealkoholické nápoje, vitamíny, Aquarius</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Vitamínové nápoje značky Aquarius.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="nealko3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">FuzeTea</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Nealkoholické nápoje, ľadový čaj, FuzeTea</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>OSviežujúce ľadové čaje značky FuzeTea.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="nealko4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Cappy džús</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Nealkoholické nápoje, džús, Cappy</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Chutné džúsy značky Cappy (plastové aj sklenené balenie). </p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/drinks/drinks%20(4).jpg">
                    </div>
                </div>
            </section>

            <section id="alko1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Rocca Forti</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, šumivé víno, Rocca Forti</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Sladké šumivé víno značky Rocca Forti s darčekovým balením.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="alko2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">JP Chenet</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, Chenet</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Medzinárodne známe a chutné víno značky Chenet</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="alko3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Šampanské Hubert</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, šampanské, Hubert</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Obľúbené šampanské značky Hubert.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="alko4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Mosler</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, sýtené víno, Ostrožovič </p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Výborné šumivé víno značky OStrožovič.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(4).jpg">
                    </div>
                </div>
            </section>
            <section id="alko5">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(5).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Karpatské Brandy Špeciál</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, KBŠ</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Kvalitné brandy zrejúce 5 rokov.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="alko6">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Vína Ostrožovič</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Alkoholické nápoje, víno, Ostrožovič</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Kvalitné vína značky Ostrožovič.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/alco/alco%20(6).jpg">
                    </div>
                </div>
            </section>

            <section id="tabak1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tabaco/tabaco%20(2).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">IQOS Heets</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Tabakové výrobky, IQOS</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Tabakové výrobky Heets pre eletronické cigarety IQOS.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tabak2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">IQOS Prislušenstvo</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Tabakové výrobky, IQOS</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Tabakové výrobky značky IQOS s možnosťou kontaktu oblastného zástupcu.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tabaco/tabaco%20(3).jpg">
                    </div>
                </div>
            </section>

            <section id="suvenir1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Šiltovky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Suveníry, šiltovky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Šiltovky s vtipným nárečovým popisom </p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="suvenir2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Hrnčeky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Plechové hrnčeky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Plechové hrnčeky s vtipným nárečovým sloganom.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(2).jpg">
                    </div>
                </div>
            </section>
            <section id="suvenir3">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(3).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Šiltovky</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Suveníry, šiltovky</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Šiltovky s vtipným nárečovým popisom </p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="suvenir4">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Zapaľovače</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Zapaľovače</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Široká ponuka rôznych vzorovaných zapaľovačov.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/suvenirs/suvenirs%20(4).jpg">
                    </div>
                </div>
            </section>

            <section id="tipos1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tipos/tipos%20(2).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Tipos</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Tipos logo</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Logo spoločnosti Tipos.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="tipos2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Tipos</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Stávkovanie, Tipos</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Rôzne lotérie spoločnosti Tipos.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/tipos/tipos%20(1).jpg">
                    </div>
                </div>
            </section>

            <section id="auto1">
                <div class="row">
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/car/car%20(1).jpg">
                    </div>
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Auto vône</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Vône do auta</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Dlhotrvajuce vôňe do auta značky Aeron. </p>
                        </div>
                    </div>
                </div>
            </section>
            <section id="auto2">
                <div class="row">
                    <div class="col-xl-6 col-lg-5 pt-5 pt-lg-0">
                        <h2 data-aos="fade-up">Dynamax pre vaše auto</h2>
                        <p data-aos="fade-up">
                        </p>
                        <div class="icon-box" data-aos="fade-up">
                            <h4>Kategória</h4>
                            <p>Starostlivosť o auto</p>
                        </div>
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <h4>Názov a popis</h4>
                            <p>Rôzne kvapaliny pre vaše vozidlo.</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-7" data-aos="fade-right">
                        <img alt="" class="img-fluid" src="../assets/img/pics/car/car%20(2).jpg">
                    </div>
                </div>
            </section>
        </div>
    </section>

</main>

<?php
include 'footer.php';
?>

<!-- JS File -->
<script src="../js/main.js"></script>

</body>
</html>