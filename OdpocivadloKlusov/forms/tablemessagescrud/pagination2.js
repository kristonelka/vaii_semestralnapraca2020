$(document).ready(function(){
    load_data();
    function load_data(page)
    {
        $.ajax({
            url:"pagination2.php",
            method:"POST",
            data:{page:page},
            success:function(data){
                $('#pagination_data2').html(data);
            }
        })
    }
    $(document).on('click', '.pagination_link', function(){
        var page = $(this).attr("id");
        load_data(page);
    });
});
