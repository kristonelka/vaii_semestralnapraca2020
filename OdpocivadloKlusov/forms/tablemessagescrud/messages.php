<?php
include '..\db_connection.php';
session_start();
?>
<!DOCTYPE html>
<html lang="sk">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Odpočívadlo Kľušov - Správy</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="../../assets/img/logo.jpg" rel="icon">
    <link href="../../assets/img/logo.jpg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Font Awesome icons -->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.13.0/js/all.js"></script>

    <!-- CSS File -->
    <link href="../../assets/css/style.css" rel="stylesheet">

</head>

<body>
<header class="fixed-top" id="header">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <h1 class="text-light"><a href="../../pages/index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="drop-down active"><a href="#">Možnosti </a>
                    <ul>
                        <li><a href="../../pages/profile.php">Osobné údaje</a></li>
                        <li><a href="../tableuserscrud/userspage.php">Registrovaní používatelia</a></li>
                        <li><a href="#">Prijaté správy</a></li>
                        <li><a href="../tablesubscrud/subscriptions.php">Aktívne odbery</a></li>
                    </ul>
                </li>
                <li><a href="../../pages/index.php"><i class="fas fa-2x fa-home"></i></a></li>
                <li><a href='../users/logout.php'><i class='fas fa fa-user-times'></i>&nbsp;&nbsp;Odhlásenie</a>
            </ul>
        </nav>
    </div>
</header>

<main id="main">
    <section class="breadcrumbs">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Prijaté správy</h2>
                <ol>
                    <li><a href="../../pages/index.php">Domov</a></li>
                    <li><a href="../../pages/profile.php">Profil</a></li>
                </ol>
            </div>
        </div>
    </section>
    <br>
    <?php
    if (isset($_GET['messageSuccess'])) {
        $message = $_GET['messageSuccess']; ?>
        <p class='alert alert-success text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    if (isset($_GET['messageError'])) {
        $message = $_GET['messageError']; ?>
        echo "<p class='alert alert-danger text-center text-uppercase font-weight-bold'><?php echo $message; ?></p>
        <?php
    }
    ?>

    <section class="profile" id="about">
        <div class="container">
            <div id="users">
                <div class="text-center">
                    <h3>Zoznam prijatých správ</h3><br/>
                    <small class="text-muted">
                        Ako admin môžete vidieť všetky správy od zákazníkov.
                    </small>
                </div>
                <br>
                <div class="table table-striped table-hover"
                     id="pagination_data2">
                </div>
            </div>
        </div>
    </section>

</main>

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Odpočívadlo Kľušov</h3>
                        <p>
                            Cintorínska 613/2<br> 085 01 Bardejov<br>
                            <strong>Telefón:</strong> +421 908 970 890<br>
                            <strong>Email:</strong> odpocivadloklusov@centrum.sk<br>
                        </p>
                        <div class="social-links mt-3">
                            <a class="facebook" href="https://www.facebook.com/odpocivadlo.klusov"><i
                                        class="bx bxl-facebook"></i></a>
                            <a class="instagram" href="https://www.instagram.com/odpocivadlo_klusov/"><i
                                        class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Užitočné linky</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#header">Domov</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#about">O nás</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#services">Ponuka</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/documents.php">Obchodné
                                podmienky</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/documents.php">Ochrana súkromia</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Naša ponuka</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#portfolio">Ako to u nás vyzerá</a>
                        </li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#services">Čo ponúkame</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Novinky</h4>

                    <div class="text-center">
                        <button data-aos="fade-up" class="btn-register" onclick="location.href='../../pages/index.php#footer'">
                            Prihláste sa na odber noviniek
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            Copyright &copy; 2020 <strong><span>Odpočívadlo Kľušov</span></strong>. Všetky práva vyhradené
        </div>
        <div class="credits">
            Designed by Kristína Pavličko
        </div>
    </div>
</footer>

<a class="back-to-top" href="#"><i class="icofont-simple-up"></i></a>

<div class="modal fade" id="modal-delete-profile-message" tabindex="-1" role="dialog"
     aria-labelledby="modalLabelDelete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelDelete">Vymazanie správy </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Určite chcete vymazať túto správu ?</p>
                <form method="POST" action="../profile/deleteprofile.php" id="form-delete-profile-message">
                    <input type="hidden" name="id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zrušiť</button>
                <button type="submit" form="form-delete-profile-message" name="profile-delete-message"
                        class="btn btn-danger">Vymazať
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Vendor JS Files -->
<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../../assets/vendor/venobox/venobox.min.js"></script>
<script src="../../assets/vendor/aos/aos.js"></script>

<!-- JS File -->
<script src="../../js/main.js"></script>
<script src="pagination2.js"></script>
<script src="../../js/delete3.js"></script>

</body>
</html>