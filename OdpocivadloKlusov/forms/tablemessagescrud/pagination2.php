<?php
include '..\db_connection.php';

$polozkyNaStranu = 6;
$strana = '';
$vystup = '';
if(isset($_POST["page"]))
{
    $strana = $_POST["page"];
}
else
{
    $strana = 1;
}

$zaciatokStrankovania = ($strana - 1)*$polozkyNaStranu;

$query = "SELECT * FROM contact ORDER BY id DESC LIMIT $zaciatokStrankovania, $polozkyNaStranu";
$result = mysqli_query($conn, $query);

$vystup .= "  
      <table class='table table-striped table-hover'>      
            <tr>
                            <th class='text-center'>ID</th>                  
                            <th class='text-center'>Meno a priezvisko</th>
                            <th class='text-center'>Email</th>
                            <th class='text-center'>Predmet</th>
                            <th class='text-center'>Správa</th>
                            <th class='text-center'>Vymazanie</th>
            </tr>
         
 ";

while($row = mysqli_fetch_array($result))
{
    $vystup .= '  
     <tr>
        <td  class="text-center">'.$row ['id'].'</td> 
        <td class="text-center">'.$row ['name'].'</td>
        <td class="text-center">'. $row ['email'].'</td>
        <td class="text-center">'. $row ['subject'].'</td>
        <td class="text-center">'. $row ['message'].'</td>
 <td class="text-center"><button class="btn-danger" name="delete-btn-profile-message" style="background: transparent; border: none;"  data-id="'.$row['id'].'" onclick="confirmDelete3(this);">
                                                    <i class="fas fa-2x fa-trash" style="color: darkred;"></i>
                                                </button></td>        
    </tr> 
      ';

}
$vystup .= '</table><br/><div align="center">';

$stranka_query = "SELECT * FROM contact ORDER BY id DESC";
$stranka_result = mysqli_query($conn, $stranka_query);

$spoluRiadkov = mysqli_num_rows($stranka_result);

$spoluStran = ceil($spoluRiadkov/$polozkyNaStranu);

for($i=1; $i<=$spoluStran; $i++)
{
    $vystup .= "<span class='pagination_link' style='cursor:pointer; padding:6px; border:1px solid #ccc;' id='".$i."'>".$i."</span>";

}
$vystup .= '</div><br /><br />';
echo $vystup;
?>