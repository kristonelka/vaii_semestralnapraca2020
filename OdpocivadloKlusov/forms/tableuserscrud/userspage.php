<?php
include '..\db_connection.php';
session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Odpočívadlo Kľušov - Používatelia</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="../../assets/img/logo.jpg" rel="icon">
    <link href="../../assets/img/logo.jpg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CRaleway:300,300i,400,400i,500,500i,600,600i,700,700i%7CPoppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="usersajax.js"></script>

    <!-- Font Awesome icons -->
    <script crossorigin="anonymous" src="https://use.fontawesome.com/releases/v5.13.0/js/all.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- CSS File -->
    <link href="../../assets/css/style.css" rel="stylesheet">


</head>
<body>

<header class="fixed-top" id="header">
    <div class="container d-flex">

        <div class="logo mr-auto">
            <h1 class="text-light"><a href="../../pages/index.php">Odpočívadlo Kľušov</a></h1>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>

                <li class="drop-down active"><a href="#">Možnosti </a>
                    <ul>
                        <li><a href="../../pages/profile.php">Osobné údaje</a></li>
                        <li><a href="#">Registrovaní používatelia</a></li>
                        <li><a href="../tablemessagescrud/messages.php">Prijaté správy</a></li>
                        <li><a href="../tablesubscrud/subscriptions.php">Aktívne odbery</a></li>
                    </ul>
                </li>
                <li><a href="../../pages/index.php"><i class="fas fa-2x fa-home"></i></a></li>
                <li><a href='../users/logout.php'><i class='fas fa fa-user-times'> </i>&nbsp;&nbsp;Odhlásenie</a>
            </ul>
        </nav>

    </div>
</header>
<main id="main">

    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Registrovaní používatelia</h2>
                <ol>
                    <li><a href="../../pages/index.php">Domov</a></li>
                    <li><a href="../../pages/profile.php">Profil</a></li>
                </ol>
            </div>

        </div>
    </section>
    <br>
    <?php
    if (isset($_GET['messageSuccess'])) {
        $message = $_GET['messageSuccess'];
        echo "<p class='alert alert-success' >" . $message . " </p>";
    }
    if (isset($_GET['messageError'])) {
        $message = $_GET['messageError'];
        echo "<p class='alert alert-danger' >" . $message . " </p>";
    }
    ?>

    <section class="profile">
        <div class="container">
            <div id="messages">
                <div class="text-center">
                    <h2>Zoznam registrovaných používateľov</h2><br/>
                    <small class="text-muted">
                        Ako admin môžete vidieť všetkých registrovaných používateľov.
                    </small>
                </div>
                <br>
                <table class="table table-striped table-hover">
                    <tr>

                        <th class="text-center">ID</th>
                        <th class="text-center">Práva</th>
                        <th class="text-center">Pohlavie</th>
                        <th class="text-center">Meno</th>
                        <th class="text-center">Priezvisko</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Používateľské meno</th>
                        <th class="text-center">Úprava/Vymazanie</th>
                    </tr>

                    <tbody>

                    <?php
                    $result = mysqli_query($conn, "SELECT * FROM users ORDER BY id DESC");

                    while ($row = mysqli_fetch_array($result)) {
                        ?>
                        <tr>

                            <td class="text-center"><?php echo $row["id"]; ?></td>
                            <td class="text-center"><?php echo $row["userType"]; ?></td>
                            <td class="text-center"><?php echo $row["gender"]; ?></td>
                            <td class="text-center"><?php echo $row["first_name"]; ?></td>
                            <td class="text-center"><?php echo $row["last_name"]; ?></td>
                            <td class="text-center"><?php echo $row["mail"]; ?></td>
                            <td class="text-center"><?php echo $row["username"]; ?></td>
                            <td class="text-center">
                                <button data-target='#editSubModal' class="edit " style="background: transparent; border: none;" data-toggle="modal">
                                    <i class="material-icons update" style="color: darkgreen;" data-toggle="fade-up"
                                       data-id="<?php echo $row["id"]; ?>"
                                       data-type="<?php echo $row["userType"]; ?>"
                                       data-gender="<?php echo $row["gender"]; ?>"
                                       data-fname="<?php echo $row["first_name"]; ?>"
                                       data-lname="<?php echo $row["last_name"]; ?>"
                                       data-email="<?php echo $row["mail"]; ?>"
                                       data-username="<?php echo $row["username"]; ?>"
                                       title="Edit"><i class="fas fa-pen"></i></i>
                                </button>
                                <?php
                                if (strpos($_SESSION['username'], $row['username']) !== false) { ?>
                                    <button disabled class="delete" style="background: transparent; border: none;" data-id="<?php echo $row["id"]; ?>"
                                       data-toggle="modal" data-target='#deleteSubModal'><i class="material-icons" style="color: darkred; opacity: 0.5;"
                                                              data-toggle="fade-up"
                                                              title="Delete"><i class="fas fa-trash"></i></i></button>
                                    <?php
                                } else { ?>
                                    <button class="delete" style="background: transparent; border: none;" data-id="<?php echo $row["id"]; ?>"
                                       data-toggle="modal" data-target='#deleteSubModal'><i class="material-icons" style="color: darkred;"
                                                              data-toggle="fade-up"
                                                              title="Delete"><i class="fas fa-trash"></i></i></button>
                                    <?php
                                } ?>

                            </td>
                        </tr>
                        <?php

                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</main>

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Odpočívadlo Kľušov</h3>
                        <p>
                            Cintorínska 613/2<br> 085 01 Bardejov<br>
                            <strong>Telefón:</strong> +421 908 970 890<br>
                            <strong>Email:</strong> odpocivadloklusov@centrum.sk<br>
                        </p>
                        <div class="social-links mt-3">
                            <a class="facebook" href="https://www.facebook.com/odpocivadlo.klusov"><i
                                        class="bx bxl-facebook"></i></a>
                            <a class="instagram" href="https://www.instagram.com/odpocivadlo_klusov/"><i
                                        class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Užitočné linky</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#header">Domov</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#about">O nás</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#services">Ponuka</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/documents.php">Obchodné
                                podmienky</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/documents.php">Ochrana súkromia</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Naša ponuka</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#portfolio">Ako to u nás
                                vyzerá</a>
                        </li>
                        <li><i class="bx bx-chevron-right"></i> <a href="../../pages/index.php#services">Čo ponúkame</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Novinky</h4>

                    <div class="text-center">
                        <button data-aos="fade-up" class="btn-register" onclick="location.href='../../pages/index.php#footer'">
                            Prihláste sa na odber noviniek
                        </button>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            Copyright &copy; 2020 <strong><span>Odpočívadlo Kľušov</span></strong>. Všetky práva vyhradené
        </div>
        <div class="credits">
            Designed by Kristína Pavličko
        </div>
    </div>
</footer>


<a class="back-to-top" href="#"><i class="icofont-simple-up"></i></a>


<!-- Edit Modal HTML -->
<div id="editSubModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="update_form">
                <div class="modal-header">
                    <h4 class="modal-title">Upraviť údaje používateľa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_u" name="id" class="form-control">
                    <?php
                    if (strpos($_SESSION['username'], "admin") !== false) { ?>
                        <div class="form-group">
                            <label>Práva</label>
                            <input type="text" id="userType_u" name="userType" class="form-control" required>
                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <label>Práva</label>
                            <input type="text" id="userType_u" name="userType" class="form-control" required readonly>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-group">
                        <label>Pohlavie</label>
                        <input type="text" id="gender_u" name="gender" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Meno</label>
                        <input type="text" id="fname_u" name="fname" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Priezvisko</label>
                        <input type="text" id="lname_u" name="lname" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" id="email_u" name="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Používateľské meno</label>
                        <input type="text" id="username_u" name="username" class="form-control" required readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" value="2" name="type">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Zrušiť">
                    <button type="button" class="btn btn-info" id="update">Uložiť úpravy</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Modal HTML -->
<div id="deleteSubModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h4 class="modal-title">Vymazať používateľa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_d" name="id" class="form-control">
                    <p>Ste si istý, že chcete vyazať tohto používateľa ?</p>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Zrušiť">
                    <button type="button" class="btn btn-danger" id="delete">Vymazať používateľa</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Vendor JS Files -->
<script src="../../assets/vendor/jquery/jquery.min.js"></script>
<script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="../../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="../../assets/vendor/venobox/venobox.min.js"></script>
<script src="../../assets/vendor/aos/aos.js"></script>

<!-- JS File -->
<script src="../../js/main.js"></script>

</body>
</html>