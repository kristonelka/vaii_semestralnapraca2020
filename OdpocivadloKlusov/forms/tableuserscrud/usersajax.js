//Edtácia
$(document).on('click','.update',function(e) {
    var id=$(this).attr("data-id");
    var type=$(this).attr("data-type");
    var gender=$(this).attr("data-gender");
    var fname=$(this).attr("data-fname");
    var lname=$(this).attr("data-lname");
    var email=$(this).attr("data-email");
    var username=$(this).attr("data-username");
    $('#id_u').val(id);
    $('#userType_u').val(type);
    $('#gender_u').val(gender);
    $('#fname_u').val(fname);
    $('#lname_u').val(lname);
    $('#email_u').val(email);
    $('#username_u').val(username);
});

$(document).on('click','#update',function(e) {
    var data = $("#update_form").serialize();
    $.ajax({
        data: data,
        type: "post",
        url: "users.php",
        success: function(dataResult){
            var dataResult = JSON.parse(dataResult);
            if(dataResult.statusCode==200){
                $('#editSubModal').modal('hide');
                location.reload();
            }
        }
    });
});

//Mazanie
$(document).on("click", ".delete", function() {
    var id=$(this).attr("data-id");
    $('#id_d').val(id);
});
$(document).on("click", "#delete", function() {
    $.ajax({
        url: "users.php",
        type: "POST",
        cache: false,
        data:{
            type:3,
            id: $("#id_d").val()
        },
        success: function(dataResult){
            $('#deleteSubModal').modal('hide');
            $("#"+dataResult).remove();
            location.reload();
        }
    });
});
