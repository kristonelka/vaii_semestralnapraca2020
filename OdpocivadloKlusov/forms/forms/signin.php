<?php
include '..\db_connection.php';
session_start();

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['pass'];

    if (empty($username) || empty($password)) {
        header("location:../../pages/forms.php?messageError=Nevyplnili ste potrebné informácie. ");
        exit();
    } else {
        $s = "select * from forms where username='$username'";
        $result = mysqli_query($conn, $s);
        $num = mysqli_num_rows($result);
        if ($num != 1) {
            header("location:../../pages/forms.php?messageError=Používateľ s týmto používateľským menom neexistuje. Pre úspešné prihlásenie je potrebné sa zaregistrovať.");
            exit();
        } else {
            $result = mysqli_query($conn, "SELECT * FROM forms WHERE username ='$username';");
            $res = mysqli_fetch_array($result);
            if (password_verify($password, $res['passsword']) !== true) {
                header("location:../../pages/forms.php?messageError=Zadali ste nesprávne heslo. V prípade, že si svoje heslo nepamätáte využite možnosť zmeny hesla.");
                exit();
            } else {
                $_SESSION['username'] =$username;
                header("location:../../pages/profile.php?messageSuccess=Prihlásenie bolo úspešné.");
                exit();
            }
        }
    }
} else {
    header("location:../../pages/forms.php");
    exit();
}

