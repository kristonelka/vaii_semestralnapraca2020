<?php
include '..\db_connection.php';

if (isset($_POST['register'])) {

    $firstname = $_POST['fname'];
    $lastname = $_POST['lname'];
    $username = $_POST['username'];
    $email = $_POST['mail'];
    $dob = $_POST['dob'];
    $password = $_POST['pass'];
    $cpassword = $_POST['cpass'];
    $gender = $_POST['gender'];
    $terms = isset($_POST['check']);
    $userType = " ";

    $s = "select * from forms where username='$username'";
    $result = mysqli_query($conn, $s);

    if (mysqli_num_rows($result) == 1) {
        header("location:../../pages/forms.php?messageError=Použivateľské meno je už zaregistrované. Ak toto používateľské meno patrí Vám, skúste sa ním prihlásiť. V opačnom prípade sa zaregistruje iným použivateľským menom.");
        exit();
    } else {
        $result = mysqli_query($conn, "select * from forms where mail='$email';");
        if (mysqli_num_rows($result) == 1) {
            header("location:../../forms/index.php?messageError=Email je už zaregistrovaný. Použite iný email.");
            exit();
        } else {
            if (strpos($username, "admin") !== false ) {
                $userType = "admin";
            } else {
                $userType = "user";
            }
            $hashedPass = password_hash($password, PASSWORD_DEFAULT);
            mysqli_query($conn, "INSERT INTO forms(first_name, last_name, mail, passsword, dob, username, userType,gender ) VALUES('$firstname', '$lastname', '$email','$hashedPass',' $dob','$username', '$userType', '$gender');");

            header("location:../../pages/forms.php?messageSuccess=Registrácia bola úspešná. Môžete sa prihlásiť.");
            exit();

        }
    }
} else {
    header("location:../../pages/forms.php");
    exit();
}
