<?php
include 'db_connection.php';

//Úprava profilu
if (isset($_POST["profile-edit"])) {
    $id = $_POST['id'];
    $username = $_POST['username'];
    $firstname = $_POST['first_name'];
    $lastname = $_POST['last_name'];
    $email = $_POST['mail'];
    $dob = $_POST['dob'];
    $image = $_POST['image'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];

    $result = $conn->query("UPDATE users SET first_name='$firstname', last_name='$lastname', mail='$email', dob='$dob', phone='$phone', address='$address' WHERE id='$id';");
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='id' value='" . $id . "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='username' value='" . $username. "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='first_name' value='" . $firstname . "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='last_name' value='" . $lastname . "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='mail' value='" . $email. "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='dob' value='" . $dob . "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='phone' value='" . $phone . "'>";
    echo "</div>";
    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='address' value='" . $address . "'>";
    echo "</div>";

    header("location:../pages/profile.php?successProfile=Profil bol úspešne upravený.");
    exit();

}

//Vymazanie profilu
else if (isset($_POST['profile-delete'])) {
    $uid = $_POST['id'];

    $result = $conn->query("DELETE FROM users WHERE id='$uid';");

    echo "        <div class='form-group'>";
    echo "            <input type='hidden' name='id' value='" . 'id' . "'>";
    echo "        </div>";

    //Odhlásenie
    session_start();
    session_unset();
    session_destroy();

    header("location:../pages/index.php?successProfile=Používateľ bol Vymazaný");
    exit();

} else {
    header("location:../pages/index.php");
    exit();
}
