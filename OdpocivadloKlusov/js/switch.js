$(document).ready(function() {
    $("#forgot-btn").click(function () {
        $("#login-box").hide();
        $("#forgot-box").show();
    });
    $("#register-btn").click(function () {
        $("#login-box").hide();
        $("#register-box").show();
    });
    $("#login-btn").click(function () {
        $("#register-box").hide();
        $("#login-box").show();
    });
    $("#back-btn").click(function () {
        $("#forgot-box").hide();
        $("#login-box").show();
    });
});