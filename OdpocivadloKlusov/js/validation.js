var registrationResult = false;
var contactResult = false;
var loginResult = false;
var changepassResult = false;
var subResult = false;
var reviewResult = false;


function checkValidationRegistration() {
    //Registracia
    var name = document.getElementById('register-name');
    var lastname = document.getElementById('register-lastname');
    var username = document.getElementById('register-username');
    var email = document.getElementById('register-email');
    var pass = document.getElementById('viewPwdLogin2');
    var cpass = document.getElementById('viewPwdLogin3');
    var conditions = document.getElementById('customCheck2');

    //meno aspoň 3 znaky
    if (name.value.length > 3) {
        //priezvisko aspoň 4 znaky
        if (lastname.value.length > 4) {
            //username aspoň 6 znakov
            if (username.value.length > 4) {
                //email pattern
                if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.value)) {
                    //heslo aspoň 6 znakov
                    if (pass.value.length > 6) {
                        //zhodné heslá
                        if (pass.localeCompare(cpass) == 0) {
                            //suhlas s podmienkami
                            if (conditions.checked) {
                                registrationResult = true;
                            }
                        }
                    }
                }
            }
        }
    } else {
        registrationResult = false;
    }
};

//--------------------------------------------------------------------------------------------------------------

function checkValidationLogin() {
    //Prihlásenie
    var uname = document.getElementById('login-username');
    var password = document.getElementById('viewPwdLogin');

    //username aspoň 6 znakov
    if (uname.value.length > 4) {
        //heslo pattern
        if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/.test(pass.value)) {
            //heslo aspoň 6 znakov
            if (password.value.length > 6) {
                loginResult = true;
            }
        }
    } else {
        loginResult = false;
    }
};

//--------------------------------------------------------------------------------------------------------------

function checkValidationChangePass() {
    //Zmena hesla
    var forgotemail = document.getElementById('forgot-email');
    var newpassword1 = document.getElementById('viewPwdLogin4');
    var newpassword2 = document.getElementById('viewPwdLogin5');

    //email pattern
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(forgotemail.value)) {
        //heslo aspoň 6 znakov
        if (newpassword1.value.length > 6) {
            //zhodne heslá
            if (newpassword1.localeCompare(newpassword2) == 0) {
                changepassResult = true;
            }
        }
    } else {
        changepassResult = false;
    }
};

//--------------------------------------------------------------------------------------------------------------

function checkValidationSub() {
    //Odber
    var submail = document.getElementById('sub-email');

    //email pattern
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(submail.value)) {
        subResult = true;
    } else {
        subResult = false;
    }
};


//--------------------------------------------------------------------------------------------------------------
function checkValidationContact() {
    //Kontakt
    var contactname = document.getElementById('contact-name');
    var contactemail = document.getElementById('contact-email');
    var contactsubject = document.getElementById('contact-subject');
    var contactmessage = document.getElementById('contact-message');


    //meno aspoň 3 znaky
    if (contactname.value.length > 5) {
        //email pattern
        if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(contactemail.value)) {
            //predmet aspoň 5 znakov
            if (contactsubject.value.length > 5) {
                //správa aspoň 15 znakov
                if (contactmessage.value.length > 5) {
                    contactResult = true;
                }
            }
        }
    } else {
        contactResult = false;
    }
};


//FUNCTIONS
//--------------------------------------------------------------------------------------------------------------

function tryRegister() {
    checkValidationRegistration();
    if (!registrationResult) {
        alert("Registrácia nie je možná! Skontrolujte správnosť a vyplnenosť všetkých polí.");
    }
    return registrationResult;
};

function tryLogin() {
    checkValidationLogin();
    if (!loginResult) {
        alert("Prihlásenie nie je možné! Skontrolujte správnosť a vyplnenosť všetkých polí.");
    }
    return loginResult;
};

function tryChangePass() {
    checkValidationChangePass();
    if (!changepassResult) {
        alert("Zmena hesla nie je možná! Skontrolujte správnosť a vyplnenosť všetkých polí.");
    }
    return changepassResult;
};

function trySub() {
    checkValidationSub();
    if (!subResult) {
        alert("Zadali ste neplatný email !");
    }
    return subResult;
};

function tryContact() {
    checkValidationContact();
    if (!contactResult) {
        alert("Kontaktný formulár nie je možné odoslať! Skontrolujte správnosť a vyplnenosť všetkých polí.");
    }
    return contactResult;
};

